#!/usr/bin/env python3
from invoke import Collection, task

from lib.tasks import dast, lint, lint_python, server, test, test_unit, zap


@task(test_unit, lint_python, default=True)
def default(context):
    pass


ns = Collection(dast=dast, default=default, lint=lint, server=server, test=test, zap=zap)

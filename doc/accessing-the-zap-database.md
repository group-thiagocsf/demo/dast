# Accessing the ZAP database

## Overview

Accessing the database created by a ZAP scan may offer the following benefits:

- Help understand what data ZAP stores, and therefore understand what data DAST can access
- Help understand the context of ZAP terms e.g. `params`
- Help understand whether settings are being applied as expected

## Security considerations

This database should not be made available to general users, as it may contain sensitive information about the application under test.

## Prerequisites

- Download the `2.5.0` version of [HSQLDB](http://hsqldb.org/)
- Extract the zip file, and locate the `hsqldb.jar` file in the `hsqldb/lib`
- Move the Jar file to a location where you will remember it, e.g. `/dev/workspace/hsqldb-2.5.0/hsqldb.jar`
- Install either the JDK 8 or JDK 11, as these are the versions compatible with HSQLDB `2.5.0`. These instructions have been tested with `openjdk version "11.0.4" 2019-07-16`.

## Steps to access the database

1. Run your DAST scan, mounting a volume to the sessions directory `docker run -ti --rm -v "$PWD/session":/home/zap/.ZAP_D/session --env DAST_WEBSITE=[target_url] dast`
1. In a terminal, run `java -jar /dev/workspace/hsqldb-2.5.0/hsqldb.jar`. The `databaseManagerSwing` application should open.
1. In the connect settings:
    - Leave `Setting Name` blank
    - Set `Type` to `HSQL Database Engine Standalone`
    - Set `Driver` to `org.hsqldb.jdbcDriver`
    - Set `URL` to `jdbc:hsqldb:file:session/dast.session`. Note that you can prepend `session/` with an absolute path if required.
    - Set `User` to `SA`
    - Leave `Password` blank
    - Click 'Ok'
1. The database should successfully connect.

![HSQLDB](img/hsqldb.png)

## Using your favourite SQL editor

You can connect to the database in any SQL editor, so long as:

- The editor understands how to connect to a database using JDBC
- The editor has access to the hsqldb.jar file

For example, using `DBeaver`, the connection can be setup as follows.

![DBeaver](img/alternate-sql-editor.png)

# Versioning

DAST uses major, minor, and patch versions, denoted by the MAJOR.MINOR.PATCH
syntax.

## What version should my work be released as?

### Major

> Breaking changes

- A change to a feature that breaks scans
- A bug fix that breaks scans
- The ZAP base image is updated to a new MAJOR version
- A variable deprecation that removes the functionality of a variable currently
  in use by customers and would result in broken scans
- Changing the OS or browser used by DAST. This could break the scans of users
  who have custom scripts
- Changes to vulnerability location definitions. Location is used to determine
  uniqueness. Changes to location break the relation between the vulnerability
  and resources (such as issues) linked to it by the user

### Minor

> New features and most other user-facing work

- A new feature is added to DAST. Prior to the change there was no way to
  achieve the same behavior
- A new feature is added to DAST. Prior to the change the same behavior could be
  achieved, but it was difficult
- New fields are added to the DAST JSON report
- The ZAP base image is updated to a new MINOR, PATCH, or weekly version
- ZAP add-ons are updated
- A variable deprecation that removes the functionality of a variable currently
  in use by customers but does _not_ result in broken scans

### Patch

> Non-breaking bug fixes

- A bug in a previously released feature is fixed
- The OS or the browser is upgraded to a more recent version
- Documentation is added under the `docs` folder (this is required in order to
  release docs-only changes)

### No version change required

- Refactors
- Changes to any tools installed on the OS (e.g. curl, Java, etc)

## Definitions

**Breaking change:** a change that breaks scans or breaks the relation between
a vulnerability and other resources, such as issues, that have been related to
it by the user in GitLab's vulnerability management tools, _or_ a change that
requires the user to take action (such as updating their DAST configuration) to
avoid such problems

**Broken scan:** a scan that is unable to generate a report with vulnerabilities
from the given target, including scans with fatal errors or scans where the
target cannot be accessed properly (for example, if requests to the target fail
because of a change in DAST, or authenticating with the target fails because of
a change in DAST). Broken scans also include scans that produce a report that
does not match the Secure Schema, or where the vulnerability data is misleading
or unusable

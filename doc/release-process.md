# DAST release process

## Versioning

DAST uses [Semantic Versioning](https://semver.org/) to communicate the kind of changes included in a release. The release script extracts the latest DAST version from the [CHANGELOG](../CHANGELOG.md).

Major changes include breaking changes to the API, such as removal of deprecated interfaces, or significant changes to the functioning of the product. Minor changes include new non-breaking features, while patch changes involve upgrading to a more recent ZAP image or fixing defects.

We recommended users pin to the Major version of DAST, for example, `registry.gitlab.com/gitlab-org/security-products/dast:1`.

## Release process

When changes are made to the DAST repo, a CI Pipeline runs that builds a DAST Docker image with the Docker tag of the Git commit SHA. Once tests pass, a DAST maintainer may promote the image into Production by triggering the `release` CI Job. The Job will add extra tags to the built image, including  `$major`, `$major.$minor`, `$major.$minor.$patch`, `latest`, and the compatible GitLab Runner e.g. `12-5-stable`. The Job also tags the Git commit with the version and creates a new GitLab release.

The new Docker images should show up in the [DAST Container Registry](https://gitlab.com/gitlab-org/security-products/dast/container_registry) in the `gitlab-org/security-products/dast/` folder.  

This process guarantees that what is released is the same as what was built and tested.

## How often should we release DAST?

As long as you feel you're releasing something important, tested and complete, please release as often as possible.

Please notify the `#g_secure-dynamic-analysis` internal Slack channel just before your release.

## GitLab Compatibility

Currently, the DAST CI template depends on `dast:$CI_SERVER_VERSION_MAJOR-$CI_SERVER_VERSION_MINOR-stable` e.g. `dast:12-5-stable`. With every new release of GitLab, there must be lock-step release of DAST with the corresponding version number. This requirement will soon no longer exist as the [CI template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml) will depend on the latest Major DAST version, e.g. `dast:1`.

Previously, releasing a new version of DAST would update all `dast:x-x-stable` tags to point to the latest version, forcing the upgrade on all users. Going forward, we will recommend automatic upgrades without forcing it to be mandatory. We can do this by leaving these tags as is and leveraging Semantic Versioning.

For example, an issue discovered in version `1.6.1` (also tagged with `12-6-stable`), and fixed in version `1.6.2` (also tagged with `12-7-stable`). Users who pin to `12-6-stable` will no longer automatically get an updated version with the included fix. Instead, we recommend they pin directly to `1` or `1.6` if they want to get these fixes. If not, they can pin directly to `1.6.1`.

## When things go wrong

- The release process needs an environment variable `GITLAB_API_TOKEN` that contains a token which has access to the GitLab API. Is it present? Has it expired?
- Releasing is not atomic; if it fails, it may be half-complete. You may end up with a Git tag for the new release, but no GitLab release. Fix the issue in the CI Job log, delete the Git tag and try again.
- Why can't I see the release CI Job? It's only available on the `master` branch. Code must be approved before it is released.
- Can we support more than one Major DAST version at the same time? Yes, but it gets complicated. Let's deal with it when it comes.

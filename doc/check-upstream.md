# Checking the Upstream version of Zed Attack Proxy (ZAP)

The DAST scanner relies on ZAP. The version of the ZAP is currently defined in the [Dockerfile](../Dockerfile).

The Zed Attack Proxy is updated on a regular basis. To find the latest version look at:
<https://github.com/zaproxy/zaproxy/tags>
and
<https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project#tab=News>

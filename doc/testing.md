# Test Documentation

## Python

DAST recommends `pyenv` for managing the version and environment of Python
which can be installed as per [their instructions](https://github.com/pyenv/pyenv#installation).

DAST can be installed by running `pyenv install $(cat .python-version)` in the root folder of your local DAST repository.

## Linting

Python files are linted with [flake8](https://pypi.org/project/flake8/) and
shell files with [shellcheck](https://github.com/koalaman/shellcheck).

On Mac OS, they can be installed with:

```sh
pip install -r ./requirements-test.txt
brew install shellcheck
```

Both linters can be run with:

```sh
./test/test-code-style.sh
```

## Unit tests

### Prerequisites

1. Setup the DAST Python environment `pip install -r ./requirements-test.txt`

### Running

- Run all unit tests with `invoke test.unit`
- Run all tests in a test file with `invoke test.unit --file [relative-path-to-file]`
- Run a single test with `invoke test.unit --test [name-of-test]`.

## Integration tests

Integration tests are broader than unit tests, and typically satisfy the following requirements:

1. They depend on more than one interesting piece of behaviour.
1. There exists a constraint that makes it not worthwhile to run on an engineer's machine prior to check-in.

Examples of constraints are:

- The time taken to run is excessive,
- Environment variables need to be set up for the test to execute, and
- Dependencies such as the Java Runtime Environment needs to be setup for the test to run.

Unit tests that start sockets or access temporary/local files are not considered integration tests
as they run extremely quickly and are not likely to interfere with an engineer's local environment.

### Prerequisites

1. Setup the DAST Python environment `pip install -r ./requirements-test.txt`
1. Setup the Java Runtime Environment
   1. Install [SDKMAN!](https://sdkman.io/) to install and manage Java versions
   1. Run `sdk list java` to see available versions of Java.
   1. Run `sdk install java [version]` to install a Java 11 version of Java. These instructions have been
tested with version `11.0.4.hs-adpt`.

### Running

- Run all unit tests with `invoke test.integration`
- Run all tests in a test file with `invoke test.integration --file [relative-path-to-file]`
- Run a single test with `invoke test.integration --test [name-of-test]`.

## End to end tests

The end to end tests use Docker to run DAST against a test target application. Note
that the tests will not build a new DAST image for you. This must be done
yourself by the following the instructions in the [README](../README.md#build-image).

To avoid noise in the test output, each test outputs the ZAP logs to
`./test/end-to-end/output/[test-name].log`. These log files are published as artifacts
in the CI job.

### Prerequisites

These tests rely on [bash\_unit](https://github.com/pgrange/bash_unit), so
begin by installing it.

To install bash\_unit on Mac OS:

```sh
bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh)
mv ./bash_unit /usr/local/bin
```

### Running

Run all E2E tests with:

```sh
docker build -t dast . && ./test/test-e2e.sh
```

#### Baseline scan test

To build DAST and run a baseline scan against a [simple web site](../test/end-to-end/fixtures/basic-site/index.html),
execute the following from the project's root directory:

```sh
docker build -t dast . && bash_unit ./test/end-to-end/test-baseline.sh
```

#### Full scan test

To build DAST and run a full scan against WebGoat 8, execute the following
from the project's root directory:

```sh
docker build -t dast . && bash_unit ./test/end-to-end/test-full-scan.sh
```

##### WebGoat

For the full scan test, WebGoat is started with a [prepopulated database](../test/end-to-end/fixtures/webgoat-data.tar.gz) containing a user.
The username is `someone` and password is `P@ssw0rd`.
The test uses these credentials to perform an authenticated scan of WebGoat.

If you want to create a different user for testing, follow these steps:

1. Start the WebGoat docker image.
1. Register a new user in WebGoat. WebGoat will persist this user in its database located at `/home/webgoat/.webgoat-8.0.0.M21/`.
1. Copy the files in `/home/webgoat/.webgoat-8.0.0.M21/`.
1. When starting a new WebGoat instance, restore the files copied in the previous step to the same directory.

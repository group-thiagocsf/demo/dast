import glob
import json
import os
import re
import shutil


class EndToEndRefresher:

    def __init__(self) -> None:
        self.end_to_end_path = os.path.realpath('./test/end-to-end')

    def execute(self):
        self._empty_output_directory()

        # build dast and dast future
        os.system('invoke dast.build dast.build-future')

        self._run_all_tests('future')
        self._run_all_tests('current')

        self._sanitize_files()
        self._copy_output_to_expect()
        print('Script Complete!')

    def _empty_output_directory(self):
        output_glob = f'{self.end_to_end_path}/output/*'

        files = glob.glob(output_glob)
        for f in files:
            os.remove(f)

    def _run_all_tests(self, test_type):
        e2e_tests = ' '.join(self._current_end_to_end_tests())
        build_image = 'dast'

        if test_type == 'future':
            e2e_tests = ' '.join(self._future_end_to_end_tests())
            build_image = 'dast-future'

        os.system(f'BUILT_IMAGE={build_image} bash_unit {e2e_tests}')

    def _current_end_to_end_tests(self):
        return [os.path.join(self.end_to_end_path, file)
                for file in os.listdir(self.end_to_end_path) if file.endswith('.sh') and 'future' not in file]

    def _future_end_to_end_tests(self):
        return [os.path.join(self.end_to_end_path, file)
                for file in os.listdir(self.end_to_end_path) if file.endswith('.sh') and 'future' in file]

    def _copy_output_to_expect(self):
        output_glob = f'{self.end_to_end_path}/output/*.json'
        files = glob.glob(output_glob)
        for f in files:
            basename = os.path.basename(f)
            new_filename = basename.replace('report_', '')
            shutil.copy(f, f'{self.end_to_end_path}/expect/{new_filename}')

    def _sanitize_files(self):
        output_glob = f'{self.end_to_end_path}/output/*.json'
        files = glob.glob(output_glob)
        for f in files:
            self._sanitze_file(f)

    def _sanitze_file(self, path):
        with open(path) as f:
            data = json.load(f)

        if 'full_scan' in path:
            if 'site' in data:
                for site in data['site']:
                    for alert in site['alerts']:
                        new_other_info = alert['otherinfo']
                        new_other_info = re.sub(r'JSESSIONID=', 'JSESSIONID=__REMOVED__', new_other_info)
                        new_other_info = re.sub(r'\[Form 1: .+ \]', '[Form 1: __REMOVED__ ]', new_other_info)
                        new_other_info = new_other_info.replace('tag [name]', 'tag [__REMOVED__]')
                        new_other_info = new_other_info.replace('tag [value]', 'tag [__REMOVED__]')
                        new_other_info = new_other_info.replace('<p>zap</p>', '<p>__REMOVED__</p>')
                        new_other_info = re.sub(r'matchingPassword=\w+', 'matchingPassword=__REMOVED__', new_other_info)
                        alert['otherinfo'] = new_other_info

            if 'vulnerabilities' in data:
                for vulnerability in data['vulnerabilities']:
                    for header in vulnerability['evidence']['request']['headers']:
                        new_value = header['value']
                        new_value = new_value.replace('JSESSIONID=********', 'JSESSIONID=__REMOVED__********')
                        header['value'] = new_value

                    for header in vulnerability['evidence']['response']['headers']:
                        new_value = header['value']
                        new_value = new_value.replace('JSESSIONID=********', 'JSESSIONID=__REMOVED__********')
                        header['value'] = new_value

                    new_summary = vulnerability['evidence']['summary']
                    new_summary = re.sub(r'\[Form 1: .+ \]', '[Form 1: __REMOVED__ ]', new_summary)
                    new_summary = new_summary.replace('tag [name]', 'tag [__REMOVED__]')
                    new_summary = new_summary.replace('tag [value]', 'tag [__REMOVED__]')
                    new_summary = re.sub(r'JSESSIONID=\w+', 'JSESSIONID=__REMOVED__', new_summary)
                    new_summary = re.sub(r'JSESSIONID=$', 'JSESSIONID=__REMOVED__', new_summary)
                    new_summary = re.sub(r'matchingPassword=\w+', 'matchingPassword=__REMOVED__', new_summary)
                    new_summary = re.sub(r'agree=agree', 'matchingPassword=__REMOVED__', new_summary)
                    new_summary = re.sub(r'password=ZAP', 'matchingPassword=__REMOVED__', new_summary)
                    new_summary = re.sub(r'username=\w+', 'matchingPassword=__REMOVED__', new_summary)
                    new_summary = re.sub(r'The user-controlled value was: \w+',
                                         'The user-controlled value was: __REMOVED__', new_summary)
                    vulnerability['evidence']['summary'] = new_summary

        with open(path, 'w') as f:
            json.dump(data, f, indent=2)

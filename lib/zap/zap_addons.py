import sys

import requests
import xmltodict

from src.system import System


class ZAPAddons:
    ADDONS_METADATA = 'https://raw.githubusercontent.com/zaproxy/zap-admin/master/ZapVersions-dev.xml#weekly'

    def __init__(self, system=System(), metadata_url=ADDONS_METADATA):
        self.system = system
        self.metadata_url = metadata_url

    def metadata(self, addon_name):
        response = requests.get(self.metadata_url, timeout=10)
        document = xmltodict.parse(response.text)

        addon_element_name = 'addon_' + addon_name

        if addon_element_name not in document['ZAP']:
            print(f"Addon '{addon_name}' not found in ZAP metadata.")
            sys.exit(1)

        addon = document['ZAP'][addon_element_name]

        self.system.notify(f'{addon_name}:')
        self.system.notify(f"  name: {addon.get('name', '')}")
        self.system.notify(f"  description: {addon.get('description', '')}")
        self.system.notify(f"  status: {addon.get('status', '')}")
        self.system.notify(f"  version: {addon.get('version', '')}")

        if 'url' in addon and 'file' in addon:
            release_page = addon['url'].replace(addon['file'], '').replace('download/', '')
            self.system.notify(f'  release page: {release_page}')

        self.system.notify(f"  url: {addon.get('url', '')}")
        self.system.notify(f"  author: {addon.get('author', '')}")
        self.system.notify(f"  hash: {addon.get('hash', '')}")
        self.system.notify(f"  info: {addon.get('info', '')}")
        self.system.notify(f"  repo: {addon.get('repo', '')}")
        self.system.notify(f"  date: {addon.get('date', '')}")
        self.system.notify(f"  size: {addon.get('size', '')}")
        self.system.notify(f"  not-before-version: {addon.get('not-before-version', '')}")

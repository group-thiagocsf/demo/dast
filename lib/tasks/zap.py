import sys

from invoke import Collection, run, task

from lib.zap import ZAPAddons, ZAPProject


@task
def metadata(context, addon=''):
    """Retrieve metadata for a ZAP Plugin (options: --addon)."""

    if not addon:
        print('Please provide the name of an add-on as an --addon argument to the task.')
        sys.exit(1)

    ZAPAddons().metadata(addon)


@task
def install(context):
    """Install a local development environment of ZAP."""
    ZAPProject(context, run).install()


@task
def update(context):
    """Update the local development environment ZAP code."""
    ZAPProject(context, run).update()


@task(install)
def start(context):
    """Run the local development environment ZAP."""
    ZAPProject(context, run).start()


@task(install)
def fetch_local_addon(context, addon=''):
    """Build a specific ZAP addon and copy the resulting .zap file to the DAST plugin directory."""
    if not addon:
        print('Please provide the name of an add-on as an --addon argument to the task.')
        sys.exit(1)

    ZAPProject(context, run).fetch_local_addon(addon)


project = Collection('project', install, update, start, fetch_local_addon)
zap = Collection(metadata, project)

from .host import Host


class BasicSiteServer:

    def __init__(self, run, port=80, secure_port=443):
        self.run = run
        self.port = port
        self.secure_port = secure_port
        self.host = Host()

    def start(self):
        self.run('docker run --rm '
                 '--name basic_site_server '
                 f'-p {self.port}:80 -p {self.secure_port}:443 '
                 '-v "${PWD}/test/end-to-end/fixtures/basic-site":/usr/share/nginx/html:ro '
                 '-v "${PWD}/test/end-to-end/fixtures/basic-site/nginx.conf":/etc/nginx/conf.d/default.conf '
                 '-v "${PWD}/test/unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt '
                 '-v "${PWD}/test/unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key '
                 '-d nginx:1.17.6')

        print(f'Basic site server started at http://{self.host.name()}:{self.port}/ '
              f'and https://{self.host.name()}:{self.secure_port}/')
        return self

    def stop(self):
        self.run('docker rm --force basic_site_server >/dev/null 2>&1 || true')
        return self

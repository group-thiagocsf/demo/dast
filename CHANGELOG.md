# GitLab DAST changelog

## v1.52.0
- Upgrade Browserker to version 0.0.31 (!452)
  - Fix a bug where cached response bodies were empty [browserker!229](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/229)
  - Users can click on elements to show the login form when authenticating using the PathToLoginForm configuration [browserker!222](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/222)
  - Redact the username and password from the JSON report [browserker!224](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/224)

## v1.51.0
- Upgrade ZAP to weekly version 2020-08-26 (!445)
- Upgrade ZAP add-on `Forced Browse` to [10.0.0](https://github.com/zaproxy/zap-extensions/releases/bruteforce-v10) (!445)
- Upgrade ZAP add-on `Common Library` to [1.2.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.2.0) (!445)
- Upgrade ZAP add-on `Fuzzer` to [13.1.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.1.0) (!445)
- Upgrade ZAP add-on `Getting Started with ZAP Guide` to [12.0.0](https://github.com/zaproxy/zap-extensions/releases/gettingStarted-v12) (!445)
- Upgrade ZAP add-on `Help - English` to [11.0.0](https://github.com/zaproxy/zap-core-help/releases/help-v11) (!445)
- Upgrade ZAP add-on `Online menus` to [8.0.0](https://github.com/zaproxy/zap-extensions/releases/onlineMenu-v8) (!445)
- Upgrade ZAP add-on `Open API Support` to [17.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v17) (!445)
- Upgrade ZAP add-on `Quick Start` to [29.0.0](https://github.com/zaproxy/zap-extensions/releases/quickstart-v29) (!445)
- Upgrade ZAP add-on `Script Console` to [27.0.0](https://github.com/zaproxy/zap-extensions/releases/scripts-v27) (!445)
- Upgrade ZAP add-on `Selenium` to [15.3.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.3.0) (!445)
- Upgrade ZAP add-on `Websockets` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v23) (!445)
- Upgrade Browserker to version 0.0.30 (!451)
  - Fix a bug where manual authentication was not reporting selector failures [browserker!209](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/209)
  - Fix a bug where large response bodies cause navigation failures [browserker!212](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/212)
  - Authentication report will output result message [browserker!215](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/215)
  - Enhance auto login to check cookie and storage value lengths [browserker!217](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/217)
  - Enhance auto login to check cookie and storage value randomness [browserker!220](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/220)
  - Advertise Browserker scanner by adding a request header to all requests [browserker!216](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/216)
- Update Browserker scans to aggregate vulnerabilities by default (!449)

## v1.50.0
- The report field `vulnerabilities[].evidence.summary` is truncated to 20,000 characters (!432)
- Remove all references to WASC from the DAST report (!442)
- Browserker scans no longer ping the target as part of the scan (!443)
- Redact `DAST_PASSWORD` from Selenium logs when debug mode is enabled (!431)
- Disable `https://www.zaproxy.org/docs/alerts/10109` for all scans (!446)
- Add `DAST_API_OPENAPI` configuration variable (!441)
- Add ZAP add-on `Access Control Testing` [6.0.0](https://github.com/zaproxy/zap-extensions/releases/accessControl-v6) (!445)
- Add ZAP add-on `Form Handler` [3.0.0](https://github.com/zaproxy/zap-extensions/releases/formhandler-v3) (!445)

## v1.49.0
- Remove auto-tagging of requests to reduce scan time (!424)
- Upgrade Browserker to version `0.0.29` (!438)
- Users can create a report to help identify authentication issues for Browserker scans (!436)

## v1.48.0
- Load external resources when running the AJAX spider (!415)
- Add `DAST_MAX_URLS_PER_VULNERABILITY` to allow users to adjust and turn off limiting for reported aggregated vulnerabilities (!429)
- Update aggregated vulnerabilities `cve` to include `-aggregated` to enable the vulnerability dashboard to differentiate the two types (!434)

## v1.47.0
- Browserker configuration options can be configured using the naming convention to `DAST_BROWSER_*` (!425)
- Redact `DAST_PASSWORD` from evidence summary (!428)

## v1.46.0
- Upgrade Browserker to version `0.0.27` (!413)
- Support `DAST_PATHS_FILE` for Browserker scans (!413)
- Add support for encoded environment variables (`DAST_PASSWORD_BASE64` and `DAST_REQUEST_HEADERS_BASE64`) for DAST on-demand scans (!418)
- User can verify authentication using a URL using `DAST_AUTH_VERIFICATION_URL` for Browserker scans (!422)
- User can verify authentication using a selector using `DAST_BROWSERKER_AUTH_VERIFICATION_SELECTOR` for Browserker scans (!422)
- User can verify authentication by detecting login forms using `DAST_BROWSERKER_AUTH_VERIFICATION_LOGIN_FORM` for Browserker scans (!422)
- Raise errors for invalid API scan configuration (!420)

## v1.45.0
- Upgrade Browserker to version `0.0.25` (!402)
- Support `DAST_PATHS` for Browserker scans (!402)
- Check `CI_PROJECT_DIR` for the file specified in `DAST_PATHS_FILE` (!399)

## v1.44.0
- Updates field `vulnerabilities.scanner` within the `gl-dast-report.json` to show as Browserker when a Browserker scan runs (!406)
- User can configure the Browserker log using `DAST_BROWSERKER_LOG` (!405)

## v1.43.0
- Update the `BrowserkerScan` to use the cookies created when Browserker authenticates when scanning (!398)
- Update Browserker to use AuthDetails instead of FormData for auth (!397)
- Enable Code Source Disclosure API rules (!403)

## v1.42.0
- Updated DAST to scan all URLs found by Browserker (!387)

## v1.41.0
- Upgrade Browserker to version `0.0.20` (!385)

## v1.40.0
- Build the next version of DAST as an alpha release (!373)
- Updated API Scanning policy rules to include higher risk checks and disable lower risk checks (!372)
- Update `gl-dast-report.json` schema version to `13.1.0` (!378)
- Upgrade Browserker to version `0.0.17` including renaming `DisableHeadless` to `Headless` (!380)

## v1.39.0
- Improved the readability of `/analyze --help` by displaying the environment variable name next to each configuration option (!374)
- Update Browserker scan to only scan hosts included in `DAST_BROWSERKER_ALLOWED_HOSTS` (!366)

## v1.38.0
- Add `DAST_EXCLUDE_URLS` which is an alias of `DAST_AUTH_EXCLUDE_URLS` (!367)

## v1.37.0
- Update `analyze` script to error if the user has insufficient permissions (!360)
- Add `DAST_AUTH_VERIFICATION_URL` to verify that DAST authentication succeeded (!364)
- Abort the scan and exit if the target returns a 500 during the access check (!363)
- Add `DAST_SKIP_TARGET_CHECK` to allow for skipping the target check (!365)

## v1.36.0
- Fix issue in Dockerfile where user was asked for input during build (!358)
- Upgrade Python to version `3.9.1` (!358)
- Upgrade Browserker to version `0.0.16` (!356)
- Browserker runs as the `gitlab` user (!356)
- Add URL validation to `DAST_WEBSITE` (!354)

## v1.35.0
- Upgrade ZAP add-on `Active scanner rules (beta)` to [32.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v32) (!352)
- Upgrade ZAP add-on `Passive scanner rules` to [30.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v30) (!352)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v23) (!352)
- Upgrade ZAP add-on `Ajax Spider` to [23.2.0](https://github.com/zaproxy/zap-extensions/releases/spiderAjax-v23.2.0) (!352)
- Upgrade ZAP add-on `Linux WebDrivers` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v23) (!352)
- Upgrade ZAP add-on `MacOS WebDrivers` to [22.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v22) (!352)
- Upgrade ZAP add-on `Windows WebDrivers` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v23) (!352)
- Upgrade ZAP add-on `Zest - Graphical Security Scripting Language` to [33.0.0](https://github.com/zaproxy/zap-extensions/releases/zest-v33) (!352)

## v1.34.0
- Add `scan.scanner.vendor.name` to the JSON report output (!334)
- Make the zap and browserker user's uid configurable at build time (!350)
- Improve `identify-addon-updates.sh` to display the changes required in the Changelog, Dockerfile and MR description (!343)
- Update release script to prevent failure when unexpected characters are included in the Changelog (!345)
- Document the new process of updating the ZAP plugins (!353)

## v1.33.0
- Improve ability to find the login button when running authenticated scans (!342)

## v1.32.1
- Update the help text for `DAST_SUBMIT_FIELD`, `DAST_FIRST_SUBMIT_FIELD`, `DAST_USERNAME_FIELD`, and `DAST_PASSWORD_FIELD` (!338)

## v1.32.0
- Revert: `Remove the duplicate target check prior to a scan (!323)` (!336)

## v1.31.0
- Remove the duplicate target check prior to a scan (!323)
- Upgrade ZAP add-on `ascanrules` to [v36.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v36) (!333)
- Upgrade ZAP add-on `ascanrulesBeta` to [v31.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v31) (!333)
- Upgrade ZAP add-on `commonlib` to [v1.1.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.1.0) (!333)
- Upgrade ZAP add-on `fuzz` to [v13.0.1](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.0.1) (!333)
- Upgrade ZAP add-on `hud` to [v0.12.0](https://github.com/zaproxy/zap-hud/releases/v0.12.0) (!333)
- Upgrade ZAP add-on `retire` to [v0.5.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.5.0) (!333)
- Upgrade ZAP add-on `webdriverlinux` to [v21.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v21) (!333)
- Upgrade ZAP add-on `webdrivermacos` to [v20.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v20) (!333)
- Upgrade ZAP add-on `webdriverwindows` to [v21.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v21) (!333)
- Upgrade ZAP add-on `websocket` to [v22.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v22) (!333)
- Disable the DAST Unix Timestamp Vulnerability Check on all scans (!335)

## v1.30.0
- Log active scan rules that were run if `DAST_DEBUG=true` (!315)
- Add `DAST_SPIDER_START_AT_HOST` to allow scans to start at given target (!317)
- Run a passive or active scan with predetermined URLs using the environment variable `DAST_PATHS_FILE` (!319)
- Prevent DAST from accessing API specification files outside of the /zap/wrk directory (!322)
- Optimize DAST image to be smaller in size (!320)
- Upgrade Firefox to version 81 (!327)
- Upgrade Geckodriver to version 0.27.0 (!320)
- Firefox runs headless when authenticating user using Selenium WebDriver (!320)

## v1.29.0
- Improve error messaging when target website check fails (!314)
- DAST can be run as a user other than `zap` for RedHat OpenShift support (!316)

## v1.28.0
- Paths added to `DAST_PATHS` are excluded if they are contained in `DAST_AUTH_EXCLUDE_URLS` (!311)
- Remove ZAP config options logging statement as DAST no longer passes control to the ZAP scripts (!313)

## v1.27.0
- Reset target to host without trailing slash to prevent duplicate vulnerabilities (!304)
- Throw an error if both the AJAX Spider and an API scan are configured (!303)
- Use a consistent strategy for handling and communicating errors (!302)
- Print the version of DAST at the start of the log (!306)
- Remove unused JAR dependencies from the Docker image that flag as an issue in some vulnerability scanners (!307)
- Promote spider logging statements to INFO (!304)

## v1.26.0
- Run a passive scan with predetermined URLs using the environment variable `DAST_PATHS` (!284)
- Run an active scan with predetermined URLs using the environment variable `DAST_PATHS` (!293) (!300)

## v1.25.0
- Add deprecation notice to -D (!291)
- Output an error when required configuration parameters have not been set (!280)
- Passive Scan rules excluded with `DAST_EXCLUDE_RULES` are marked as skipped in the log summary (!290)
- Fix bug where headers that contain colons cause DAST to crash (!296)
- Set site availability timeout using `DAST_TARGET_AVAILABILITY_TIMEOUT` environment variable (!287)

## v1.24.0
- Fix bug where `DAST_DEBUG` is ignored (!285)
- Improve logging output during active scan (!273)
- Log warning when `ZAP_API_HOST_OVERRIDE` used when importing an API specification from a file (!274)
- Results printed at the end of a scan only include rules that were run (!268)
- DAST exits with a non-zero exit code when ZAP crashes (!265)
- Errors thrown during a scan cause the scan to abort (!264)
- Remove tagging of URLs because they take a long time to execute and don't appear to be used (!229)
- Show skipped rules in log output (!269)

## v1.23.0
- Set the directory location of custom ZAP scripts using `DAST_SCRIPT_DIRS` (!234)
- Add deprecation notice to `-p` configuration option (!246)
- Add deprecation notice to the `-n` configuration option (!252)
- Set how long for DAST to wait for a passive scan to execute with the environment variable `DAST_PASSIVE_SCAN_MAX_WAIT_TIME` (!252)
- Set how many attempts DAST should make to connect to the ZAP Server with the environment variable `DAST_ZAP_MAX_CONNECTION_ATTEMPTS` (!252)
- Set how many seconds DAST should sleep in between ZAP server connection attempts with the environment variable `DAST_ZAP_CONNECT_SLEEP_SECONDS` (!252)

## v1.22.1
- Use an explicit session name to ensure the correct ZAP database is used (!241)

## v1.22.0
- Remove duplicate request for ZAP alerts to reduce memory usage and time to build report (!239)
- Retrieve ZAP messages for alerts from the ZAP database to reduce the memory required to run DAST (!239)
- Retrieve ZAP messages for scanned resources from the ZAP database to reduce the memory required to run DAST (!240)

## v1.21.1
- Add deprecation notice to `-s` configuration option (!219)
- HSQLDB logs are logged at `WARN` to reduce noise in the log file (!222)

## v1.20.0
- Rules excluded using the `DAST_EXCLUDE_RULES` environment variable do not run (!216)

## v1.19.0
- Update DAST to use the ZAP Docker base image `owasp/zap2docker-weekly:w2020-06-30` (!208)
- Upgrade Python to version `3.8.2` (!208)
- Prevent ZAP from failing when running in other container engines than Docker (!197)
- Upgrade ZAP add-on `ascanrules` to [v35.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v35) (!211)
- Upgrade ZAP add-on `ascanrulesBeta` to [v28.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v28) (!211)
- Upgrade ZAP add-on `commonlib` to [v1.0.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.0.0) (!211)
- Upgrade ZAP add-on `fuzzdb` to [v7.0.0](https://github.com/zaproxy/zap-extensions/releases/fuzzdb-v7) (!211)
- Upgrade ZAP add-on `openapi` to [v16.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v16) (!211)
- Upgrade ZAP add-on `pscanrules` to [v29.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v29) (!211)
- Upgrade ZAP add-on `pscanrulesBeta` to [v22.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v22) (!211)
- Upgrade ZAP add-on `webdriverlinux` to [v18.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v18) (!211)
- Upgrade ZAP add-on `webdrivermacos` to [v17.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v17) (!211)
- Upgrade ZAP add-on `webdriverwindows` to [v18.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v18) (!211)

## v1.18.1
- Reduce memory usage by excluding HTTP request and response bodies from alerts (!201)

## v1.18.0
- Remove support for the ZAP config file because it doesn't work as intended (!185)
- Remove environment variables `DAST_ZAP_CONFIG_FILE`, `DAST_ZAP_CONFIG_URL`, `DAST_ZAP_GENERATE_CONFIG` (!185)
- Run DAST as the `zap` user, not as `root` (!186)
- Update Firefox to version `77.0` and Geckodriver to version `0.26.0` (!106)
- Update Python Selenium to version `3.141.0` (!191)

## v1.17.1
- Default spidering in a full scan to have an unlimited maximum duration (!184)

## v1.17.0
- Remove `INFO` log4j properties from base logging configuration for ZAP Server (!182)
- Include request and response headers in the vulnerability evidence (!168)
- Mask sensitive request and response header values with environment variable DAST_MASK_HTTP_HEADERS (!179)

## v1.16.0
- Set ZAP Server log4j log levels using `DAST_ZAP_LOG_CONFIGURATION` (!166)
- Remove GitLab Runner style `x-x-stable` tags from future DAST Docker images (!174)
- Remove legacy DAST entrypoints, please use `/analyze` instead (!172)

## v1.15.0
- Include context of the scan in the DAST JSON Report (!165)

## v1.14.0
- The DAST JSON report is created using information from the ZAP REST API, not the ZAP JSON report (!142)
- Set the maximum duration of the spider scan with environment variable `DAST_SPIDER_MINS` (!153)
- Include alpha passive and active scan rules with environment variable `DAST_INCLUDE_ALPHA_VULNERABILITIES` (!153)
- Set the ZAP config URL to configure vulnerability finding risk levels with environment variable `DAST_ZAP_CONFIG_URL` (!153)
- Set the name of the ZAP config file to configure vulnerability finding risk levels with environment variable `DAST_ZAP_CONFIG_FILE` (!163)
- Generate sample config file with environment variable `DAST_ZAP_GENERATE_CONFIG` (!163)
- Set the ZAP Server command-line options with environment variable  `DAST_ZAP_CLI_OPTIONS` (!163)
- Enable DAST debug messages with environment variable `DAST_DEBUG` (!163)
- Set the file name of the ZAP HTML report written at the end of a scan using `DAST_HTML_REPORT` (!159)
- Set the file name of the ZAP Markdown report written at the end of a scan using `DAST_MARKDOWN_REPORT` (!159)
- Set the file name of the ZAP XML report written at the end of a scan using `DAST_XML_REPORT` (!159)
- Copy contents of `/zap/wrk` to the working directory in order to make them available as CI job artifacts (!160)

## v1.13.3
- Enable Ajax spider using the environment variable `DAST_ZAP_USE_AJAX_SPIDER` (!147)

## v1.13.2
- ZAP does not make external HTTP requests when `auto-update-addons` is false (!146)
- Upgrade ZAP add-on `fuzzdb` to [v6](https://github.com/zaproxy/zap-extensions/releases/tag/fuzzdb-v6) (!144)
- Upgrade ZAP add-on `pscanrules-release` to [v28](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v28) (!144)
- Upgrade ZAP add-on `selenium-release` to [v15.2.0](https://github.com/zaproxy/zap-extensions/releases/tag/selenium-v15.2.0) (!144)

## v1.13.1
- Disable auto-updating ZAP addons by default (!143)

## v1.13.0
- Each Vulnerability is now identifiable by a unique id in the JSON report (!128)

## v1.12.1
- Rename `CommonReportFormatter` to `SecurityReportFormatter` (!138)

## v1.12.0
- `vulnerabilities[].evidence.summary` is added to the report to provide more context about the vulnerability (!132)

## v1.11.1
- `severity` and `confidence` report values are title case to match the DAST schema (!131)

## v1.11.0
- OpenAPI specifications can be referenced using the `DAST_API_SPECIFICATION` environment variable to trigger an API scan (!92)(!112)(!125)(!126)
- Hosts defined in API specifications can be overridden using the `DAST_API_HOST_OVERRIDE` environment variable (!112)(!123)
- Validation rules can be excluded from the scan using the `DAST_EXCLUDE_RULES` environment variable (!115)
- Request headers can be added to every request using the `DAST_REQUEST_HEADERS` environment variable (!124)

## v1.10.0
- Limit URLs displayed in console output to those captured during spider scan (!113)
- Output URLs spidered by DAST JSON report as `scan.scanned_resources` (!113)
- Logging is `INFO` level by default (!110)

## v1.9.0
- Include URLs scanned by DAST in the console output (!112)

## v1.8.0
- DAST depends on ZAP weekly release w2020-02-10 (!101)
- DAST Python scripts run using Python 3.6.9 in anticipation of Python 2 EOL (!102)
- Upgrade pinned add-on versions: alertFilters to [v10](https://github.com/zaproxy/zap-extensions/releases/tag/alertFilters-v10), ascanrulesBeta to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/ascanrulesBeta-v27) and pscanrules to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v27) (!107)

## v1.7.0
- Include recent ZAP add-ons into the DAST Docker image so they don't have to be downloaded every scan (!105)
- Auto-update of ZAP add-ons can be disabled with the `--auto-update-addons false` command line argument (!105)

## v1.6.1
- Fix issue where AJAX spider scans could not start Firefox (!87)

## v1.6.0
- Add initial Secure Common Report Format fields to DAST report (!81)

## v1.5.4
- Validate URL command line arguments (!69)
- Exit code is zero when a scan runs successfully, non zero when a scan fails or arguments are invalid (!69)
- The DAST report is deterministic, keys in the JSON document are in alphabetical order (!70)
- Ajax scans start from the target URL (!71)
- Don't verify HTTPS certificates when determining if a target site is ready for scanning (!76)

## v1.5.3
- Fixed support for `--auth-exclude-urls` parameter (!52)

## v1.5.2
- DAST depends on ZAP weekly release w2019-09-24, re-enabling `ascanrulesBeta` rules
- Removed Python 3 to fix Full Scans

## v1.5.1
- DAST depends on ZAP release 2.8.0 (!50)

## v1.5.0
- Running `/analyze --help` shows all options and environment variables supported by DAST (!39)
- Expose ZAP logs while executing scans (!42)

## v1.4.0
- Implement [domain validation](https://docs.gitlab.com/ee/user/application_security/dast/index.html#domain-validation) option for full scans (!35)

## v1.3.0
- Report which URLs were scanned (!24)

## v1.2.7
- Fix passing of optional params to ZAP (!27)

## v1.2.6
- Fix max. curl timeout to be longer than 150 seconds (!26)

## v1.2.5
- Fix curl timeout (!25)

## v1.2.4
- Fix timeout when $DAST_TARGET_AVAILABILITY_TIMEOUT is used (!21)

## v1.2.3
- Fix auto login functionality. Auto login is used if the HTML elements for username, password, or submit button have not been specified.

## v1.2.2
- Fix a bug where `analyze` would fail if only `DAST_WEBSITE` was used. https://gitlab.com/gitlab-org/gitlab-ee/issues/11744

## v1.2.1
- Accept $DAST_WEBSITE env var instead of `-t` parameter (still supported for backward compatibility)

## v1.2.0
- Add [ZAP Full Scan](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan) support (!14)

## v1.1.2
- Add workaround for supporting long CLI auth options (!9)

## v1.1.1
- Fix a problem with multiple login buttons on the login page.

## v1.1.0
- First release of the DAST GitLab image.

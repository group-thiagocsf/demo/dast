import logging

from src.configuration import Configuration
from src.custom_hooks import CustomHooks
from src.system import System
from src.zap_gateway import ZAPServer, ZAProxy
from src.zap_log_configuration import ZAPLogConfiguration


class ScanScriptWrapper():

    def __init__(self, config: Configuration, zap_server: ZAPServer, zaproxy: ZAProxy, custom_hooks: CustomHooks):
        self.config = config
        self.system = System()
        self.zap_server = zap_server
        self._zap_log_configuration = ZAPLogConfiguration(config)
        self._zaproxy = zaproxy
        self._custom_hooks = custom_hooks

    def run(self) -> None:
        logging.info(f'Running DAST v{self.system.dast_version()} on Python {self.system.python_version()}')

        logging.debug('writing zap log configuration')
        self._write_zap_log_configuration()

        # Hide "Starting new HTTP connection" messages
        logging.getLogger('requests').setLevel(logging.DEBUG)

        zap_daemon = self.zap_server.start()
        zap_client = self._zaproxy.connect(zap_daemon.proxy_endpoint())

        self._custom_hooks.handover_to_dast(zap_client, zap_daemon)

    def _write_zap_log_configuration(self) -> None:
        with open(self._zap_log_configuration.get_path(), 'w+') as handle:
            self._zap_log_configuration.write_log_properties(handle)

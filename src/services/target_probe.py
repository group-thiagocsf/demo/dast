import logging
from typing import Optional

from requests import Response, get
from requests.exceptions import ConnectionError, \
                                HTTPError, \
                                ProxyError, \
                                ReadTimeout, \
                                RequestException
from requests.packages import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from src.configuration import Configuration
from .target_availability_check import TargetAvailabilityCheck


class TargetProbe:

    CHECK_ERROR_MESSAGES = {
        ConnectionError: 'failed to connect to target',
        ProxyError: 'failed to connect to ZAP',
        ReadTimeout: 'request timed out while waiting for data from server',
    }

    REQUEST_TIMEOUT = 5

    def __init__(
        self, target: str, config: Configuration,
        proxy: Optional[str] = None,
        follow_redirects: bool = True,
    ):
        self._config = config
        self._target = target
        self._proxy = proxy
        self._follow_redirects = follow_redirects

    def send_ping(self) -> TargetAvailabilityCheck:
        # thank you, we know that certificates should ideally be validated
        urllib3.disable_warnings(InsecureRequestWarning)

        try:
            logging.info(f'Requesting access to {self._target}...')

            if self._proxy:
                proxies = {'http': self._proxy, 'https': self._proxy}
                response = get(
                    self._target, proxies=proxies, timeout=self.REQUEST_TIMEOUT, verify=False,
                    allow_redirects=self._follow_redirects,
                )
            else:
                response = get(
                    self._target, timeout=self.REQUEST_TIMEOUT, verify=False,
                    allow_redirects=self._follow_redirects,
                )

            return self._check_status(response)
        except (ConnectionError, ProxyError, ReadTimeout, RequestException) as error:
            error_message = self.CHECK_ERROR_MESSAGES.get(type(error), 'request failed')

            logging.info(f'{type(error).__name__}: {error_message}')

            return TargetAvailabilityCheck(False, self._config, unavailable_reason=error)

    def _check_status(self, response: Response) -> TargetAvailabilityCheck:
        try:
            response.raise_for_status()
        except HTTPError as error:
            logging.info(f'{type(error).__name__}: {response.status_code}')
            logging.debug(response.text)

            return TargetAvailabilityCheck(
                False, self._config, response=response, unavailable_reason=error,
            )
        else:
            return TargetAvailabilityCheck(True, self._config, response=response)

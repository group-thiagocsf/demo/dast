from os import listdir, path
from typing import List

from src.models import Script, ScriptLanguage, ScriptType


class ScriptFinder:

    def __init__(self, script_dirs: List[str]):
        self._script_dirs = script_dirs

    def find_scripts(self) -> List[Script]:
        scripts = []

        for script_dir in self._script_dirs:
            for script_type in ScriptType:
                scripts.extend(self._find_scripts_of_type(script_dir, script_type))

        return scripts

    def _find_scripts_of_type(self, directory: str, script_type: ScriptType) -> List[Script]:
        base_dir = path.join(directory, script_type.zap_name())

        if not path.isdir(base_dir):
            return []

        files = [f'{base_dir}/{filename}' for filename in listdir(base_dir)]
        script_files = [file for file in files if self._file_is_valid_script(file)]

        return [self._build_script(script_type, file_path) for file_path in script_files]

    def _build_script(self, script_type: ScriptType, file_path: str) -> Script:
        script_name = path.basename(file_path)
        return Script(script_type, script_name, str(file_path))

    def _file_is_valid_script(self, file: str) -> bool:
        return ScriptLanguage.detect_language(file) and path.isfile(file)

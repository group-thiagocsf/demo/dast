import logging
from os import linesep
from typing import Callable, Dict, List

from src.configuration import Configuration
from src.zap_gateway import Settings


class BrowserkerConfigurationFile:

    def __init__(self, config: Configuration, file_path: str):
        self._config = config
        self._file_path = file_path

    def write(self) -> None:
        allowed_hosts = self.list_values(self._config.browserker_allowed_hosts)
        excluded_uris = self.list_values(self._config.exclude_urls, lambda url: url != self._config.auth_url)

        settings = [
            f'AllowedHosts = [{allowed_hosts}]',
            'DataPath = "/output/browserker_data"',
            f'ExcludedElements = [{self.list_values(self._config.browserker_excluded_elements)}]',
            f'ExcludedHosts = [{self.list_values(self._config.browserker_excluded_hosts)}]',
            f'ExcludedURIs = [{excluded_uris}]',
            'FileLogPath = "/output/browserker-debug.log"',
            f'IgnoredHosts = [{self.list_values(self._config.browserker_ignored_hosts)}]',
            'JSPluginPath = "/browserker/plugins/"',
            f'MaxActions = {self._config.browserker_max_actions}',
            f'MaxAttackFailures = {self._config.browserker_max_attack_failures}',
            f'MaxDepth = {self._config.browserker_max_depth}',
            f'NumBrowsers = {self._config.browserker_number_of_browsers}',
            f'Proxy = "http://127.0.0.1:{self._config.zap_port}"',
            'PluginResourcePath = "/browserker/resources/"',
            'ReportCookiesPath = "/output/cookies.json"',
            'ScanMode = "crawl"',
            'ShowBrowser = false',
            f'URL = "{self._config.target}"',
        ]

        if self._config.paths_to_scan_list:
            formatted_paths = [f'"{path}"' for path in self._config.paths_to_scan_list]
            settings.append(f'DirectPaths = [{",".join(formatted_paths)}]')

        if self._config.paths_to_scan_file_path:
            settings.append(f'DirectPathsFilePath = "{self._config.paths_to_scan_file_path}"')

        settings.extend(self.mapped_values('CustomHeaders', self._config.request_headers))
        settings.extend(self.mapped_values('CustomCookies', self._config.browserker_cookies))
        settings.extend(self.mapped_values('FileLogLevels', {'LogLevel': 'debug'}))
        settings.extend(self.mapped_values('ConsoleLogLevels', self.sanitize_log_values(self._config.browserker_log)))

        if self.has_auth_details():
            settings.extend([
                '',
                '[AuthDetails]',
                f'LoginURL = "{self._config.auth_url}"',
                f'UserName = "{self._config.auth_username}"',
                f'Password = "{self._config.auth_password}"',
            ])

            if self._config.auth_username_field:
                settings.append(f'UserNameField = "{self._config.auth_username_field}"')
            if self._config.auth_first_submit_field:
                settings.append(f'UserNameSubmitField = "{self._config.auth_first_submit_field}"')
            if self._config.auth_password_field:
                settings.append(f'PasswordField = "{self._config.auth_password_field}"')
            if self._config.auth_submit_field:
                settings.append(f'SubmitButtonField = "{self._config.auth_submit_field}"')
            if self._config.auth_verification_url:
                settings.append(f'VerificationURL = "{self._config.auth_verification_url}"')
            if self._config.auth_verification_selector:
                settings.append(f'VerificationSelector = "{self._config.auth_verification_selector}"')
            if self._config.auth_verification_login_form:
                settings.append('VerificationLoginForm = true')
            if self._config.auth_report:
                settings.append(f'ReportPath = "{Settings.WRK_DIR}gl-dast-debug-auth-report.html"')

        for setting in settings:
            logging.info(f'Adding Browserker setting {setting}')

        with open(self._file_path, 'w') as file:
            for setting in settings:
                file.write(f'{setting}{linesep}')

    def has_auth_details(self) -> bool:
        return self._config.auth_url is not None \
            and self._config.auth_username \
            and self._config.auth_password

    def list_values(self, values_list: List[str], url_acceptable: Callable[[str], bool] = lambda url: True) -> str:
        if not values_list:
            return ''

        values = [url for url in values_list if url and url_acceptable(url)]

        if not values:
            return ''

        comma_separated = '", "'.join(values)
        return f'"{comma_separated}"'

    def mapped_values(self, name: str, values: Dict[str, str]) -> List[str]:
        if not values:
            return []

        settings = ['', f'[{name}]']

        for key, value in values.items():
            settings.extend([f'{key} = "{value}"'])

        return settings

    def sanitize_log_values(self, moduleLevels: Dict[str, str]) -> Dict[str, str]:
        sanitized = {'LogLevel': 'info'}

        for module, level in (moduleLevels or {}).items():
            sanitized[module.upper()] = level.lower()

        return sanitized

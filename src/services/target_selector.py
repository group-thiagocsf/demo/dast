import logging

from src.models import Target
from src.zap_gateway import ZAProxy
from .api_specification import APISpecification
from .api_target_selector import APITargetSelector
from ..configuration import Configuration


class TargetSelector:

    def __init__(self, zaproxy: ZAProxy, config: Configuration):
        self._zap = zaproxy
        self._config = config

    def select(self) -> Target:
        target = Target(self._config.target)

        if self._config.is_api_scan:
            api_urls = APISpecification(
                self._zap,
                str(self._config.api_specification),
                self._config.zap_api_host_override,
            ).load()

            target = APITargetSelector(api_urls, self._config.zap_api_host_override).select()

        if self._config.spider_start_at_host and not target.is_host_url():
            target = target.reset_to_host()

            logging.debug(f'Setting target to normalized URL: {target}')

        logging.info(f'Using scan target {target}')

        return target

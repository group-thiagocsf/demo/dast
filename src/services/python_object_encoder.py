from json import JSONEncoder
from typing import Any


class PythonObjectEncoder(JSONEncoder):

    def default(self, obj: Any) -> Any:
        return obj.__json__()

import logging
from datetime import datetime, timedelta
from time import sleep

from src.configuration import Configuration
from src.models.errors import TargetNotAccessibleError
from .target_availability_check import TargetAvailabilityCheck
from .target_probe import TargetProbe


class TargetAvailability:

    SERVER_ERROR_CODES = range(500, 599)
    RETRY_DELAY = 3  # seconds

    def __init__(self, target: str, config: Configuration):
        self._target = target
        self._config = config

    def verify(self) -> None:
        check = self._check_site_is_available()
        is_safe_to_scan, safety_error = check.is_safe_to_scan()

        if not check.is_available() and check.status_code() in self.SERVER_ERROR_CODES:
            raise TargetNotAccessibleError(
                f'Target access check failed with HTTP error code: {check.status_code()}. Canceling scan. '
                'Set the variable DAST_SKIP_TARGET_CHECK to `true` to prevent this check',
            )
        elif not check.is_available():
            logging.info(f'Last attempted access of target caused error: {check.unavailable_reason()}')
            logging.warning(f'{self._target} could not be reached, attempting scan anyway')
        elif not is_safe_to_scan:
            domain_validation_url = 'https://docs.gitlab.com/ee/user/application_security/dast/#domain-validation'
            raise TargetNotAccessibleError(
                f'Domain validation failed due to: {safety_error}, see {domain_validation_url}',
            )

    def _check_site_is_available(self) -> TargetAvailabilityCheck:
        logging.info(f'Waiting for {self._target} to be available')

        check = TargetAvailabilityCheck(False, self._config)
        max_time = datetime.utcnow() + timedelta(0, self._config.availability_timeout)

        while datetime.utcnow() < max_time:
            check = TargetProbe(self._target, self._config).send_ping()

            if check.is_available():
                break

            if datetime.utcnow() + timedelta(0, self.RETRY_DELAY) > max_time:
                break

            sleep(self.RETRY_DELAY)

        return check

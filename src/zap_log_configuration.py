class ZAPLogConfiguration:

    PATH = '/app/zap/log4j.properties'

    HEADER = [
        '# Properties below added programmtically by DAST',
    ]

    BASE_PROPERTIES = [
        'log4j.rootLogger=INFO, R',
        'log4j.appender.R=org.apache.log4j.RollingFileAppender',
        'log4j.appender.R.File=${zap.user.log}/zap.log',
        'log4j.appender.R.MaxFileSize=1024MB',
        'log4j.appender.R.MaxBackupIndex=3',
        'log4j.appender.R.layout=org.apache.log4j.PatternLayout',
        'log4j.appender.R.layout.ConversionPattern=%d [%-5t] %-5p %c{1} - %m%n',
        'log4j.logger.org.apache.commons.httpclient=ERROR',
        # Disable Jericho log, it logs HTML parsing issues as errors.
        'log4j.logger.net.htmlparser.jericho=OFF',
        # Prevent Crawljax from logging too many, not so useful, INFO messages.,
        # For example:,
        # INFO  Crawler - New DOM is a new state! crawl depth is now 10,
        # INFO  Crawler - Crawl depth is now 1,
        # INFO  Crawler - Crawl depth is now 2,
        # INFO  UnfiredCandidateActions - There are 64 states with unfired actions,
        # INFO  StateMachine - State state106 added to the StateMachine.,
        'log4j.logger.com.crawljax.core.Crawler = WARN',
        'log4j.logger.com.crawljax.core.state.StateMachine = WARN',
        'log4j.logger.com.crawljax.core.UnfiredCandidateActions = WARN',
        'log4j.logger.hsqldb.db.HSQLDB379AF3DEBD.ENGINE = WARN',
    ]

    def __init__(self, config):
        self.config = config

    def get_path(self):
        return self.PATH

    def write_log_properties(self, io):
        contents = '\n'.join(self.HEADER + self.BASE_PROPERTIES + self._get_extra_properties())
        io.write(contents)

    def _get_extra_properties(self):
        properties = str(self.config.zap_log_configuration or '').split(';')
        return list(filter(None, properties))

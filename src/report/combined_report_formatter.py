from collections import OrderedDict


class CombinedReportFormatter:

    def __init__(self, *formatters):
        self.formatters = formatters

    def format_report(self, zap_version, alerts, scanned_resources, end_date_time, spider_scan, spider_scan_results):
        reports = [formatter.format_report(
            zap_version, alerts, scanned_resources, end_date_time, spider_scan, spider_scan_results)
            for formatter in self.formatters]

        combined = {}

        for report in reports:
            for key in report.keys():
                if key in combined.keys():
                    raise RuntimeError('Cannot create combined report as '
                                       f"multiple formatters produce property '{key}'")

            combined.update(report)

        return OrderedDict(sorted(combined.items(), key=lambda t: t[0]))

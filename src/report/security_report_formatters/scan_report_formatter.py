from collections import OrderedDict
from datetime import datetime
from typing import Any, Dict, List

from src import Configuration
from src.models.http import HttpMessages

ScannedResource = Dict[str, str]


class ScanReportFormatter:

    def __init__(self, start_date_time: datetime, config: Configuration):
        self._config = config
        self._start_date_time = start_date_time

    def format_report(self, scanned_resources: HttpMessages,
                      zap_version: str,
                      end_date_time: datetime) -> Dict[str, Any]:
        formatted = OrderedDict()
        formatted['end_time'] = self._format_date_time(end_date_time)
        formatted['messages'] = []
        formatted['scanned_resources'] = self._format_scanned_resources(scanned_resources)
        formatted['scanner'] = self._format_scanner(zap_version)
        formatted['start_time'] = self._format_date_time(self._start_date_time)
        formatted['status'] = 'success'
        formatted['type'] = 'dast'
        return formatted

    def _format_date_time(self, date_time: datetime) -> str:
        return date_time.strftime('%Y-%m-%dT%H:%M:%S')

    def _format_scanner(self, zap_version: str) -> Dict[str, str]:
        formatted = OrderedDict()

        if self._config.browserker_scan:
            formatted['id'] = 'zaproxy-browserker'
            formatted['name'] = 'OWASP Zed Attack Proxy (ZAP) and Browserker'
        else:
            formatted['id'] = 'zaproxy'
            formatted['name'] = 'OWASP Zed Attack Proxy (ZAP)'

        formatted['url'] = 'https://www.zaproxy.org'
        formatted['version'] = zap_version
        formatted['vendor'] = OrderedDict()
        formatted['vendor']['name'] = 'GitLab'
        return formatted

    def _format_scanned_resources(self, scanned_resources: HttpMessages) -> List[ScannedResource]:
        resources = scanned_resources.request_summaries()
        scanned_resources = [self._format_scanned_resource(resource) for resource in resources]

        sorted_scanned_resources = sorted(scanned_resources, key=lambda resource: (resource['url'], resource['method']))
        return sorted_scanned_resources

    def _format_scanned_resource(self, scanned_resource: Dict[str, str]) -> ScannedResource:
        formatted = OrderedDict()
        formatted['method'] = scanned_resource['method']
        formatted['type'] = 'url'
        formatted['url'] = scanned_resource['url']
        return formatted

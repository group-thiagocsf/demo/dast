from __future__ import annotations

from typing import Any, Dict, List, NewType, Optional

from .cwe import CweID
from .http import HttpMessage
from .id import ID
from .rule import RuleID
from .source import SourceID

AlertProperty = NewType('AlertProperty', str)


class AlertID(ID):
    pass


class Alert:
    RISK_CODES = {'Informational': '0', 'Low': '1', 'Medium': '2', 'High': '3'}

    def __init__(self, alert: Dict[str, Any]):
        self._alert = alert

    @property
    def id(self) -> AlertID:  # noqa: A003
        return AlertID(self._alert['id'])

    @property
    def cwe_id(self) -> CweID:
        return CweID(self._alert.get('cweid', ''))

    @property
    def source_id(self) -> SourceID:
        return SourceID(self._alert.get('sourceid', ''))

    @property
    def rule_id(self) -> RuleID:
        return RuleID(self._alert.get('pluginId', ''))

    @property
    def message(self) -> Optional[HttpMessage]:
        if self._alert.get('message'):
            message: HttpMessage = self._alert['message']

            return message

        return None

    @property
    def url(self) -> str:
        return self._alert.get('url', '')

    @property
    def response_code(self) -> Optional[int]:
        if self.message is None:
            return None

        return int(self.message.response.status)

    @property
    def detail(self) -> str:
        return self._alert.get('other', '')

    @property
    def confidence(self) -> str:
        return self._alert.get('confidence', '')

    @property
    def description(self) -> str:
        return self._alert.get('description', '')

    @property
    def risk(self) -> str:
        return self._alert.get('risk', '')

    @property
    def riskcode(self) -> str:
        risk_name = self._alert.get('risk', '')

        if risk_name == '':
            return self.RISK_CODES['Informational']

        if risk_name not in self.RISK_CODES.keys():
            return ''

        return self.RISK_CODES[risk_name]

    @property
    def evidence(self) -> str:
        return self._alert.get('evidence', '')

    @property
    def name(self) -> str:
        return self._alert.get('name', '')

    @property
    def method(self) -> str:
        return self._alert.get('method', '')

    @property
    def param(self) -> str:
        return self._alert.get('param', '')

    @property
    def reference(self) -> str:
        return self._alert.get('reference', '')

    @property
    def solution(self) -> str:
        return self._alert.get('solution', '')

    @property
    def attack(self) -> str:
        return self._alert.get('attack', '')

    @property
    def is_aggregated(self) -> bool:
        return False

    def aggregated_alerts(self) -> List[Alert]:
        return [self]

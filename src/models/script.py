from os import path

from .script_type import ScriptType


class Script:

    def __init__(self, script_type: ScriptType, name: str, file_path: str):
        self._script_type = script_type
        self._name = name
        self._file_path = file_path

    @property
    def script_type(self) -> ScriptType:
        return self._script_type

    @property
    def name(self) -> str:
        return self._name

    @property
    def file_path(self) -> str:
        return self._file_path

    def exists(self) -> bool:
        return path.isfile(self._file_path)

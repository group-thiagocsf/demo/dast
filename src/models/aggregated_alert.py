from typing import List

from .alert import Alert


class AggregatedAlert(Alert):

    def __init__(self, alerts: List[Alert]):
        super().__init__(alerts[0]._alert)
        self._alerts = alerts

    @property
    def is_aggregated(self) -> bool:
        return True

    def aggregated_alerts(self) -> List[Alert]:
        return self._alerts

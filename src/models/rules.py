from __future__ import annotations

from typing import List

from .rule import Rule


class Rules:

    def __init__(self, rules: List[Rule]):
        self.__rules = rules

    def __getitem__(self, key) -> Rule:
        return self.__rules[key]

    def __len__(self) -> int:
        return len(self.__rules)

    def disabled(self) -> Rules:
        return Rules([rule for rule in self.__rules if not rule.is_enabled()])

from typing import TypedDict

from .id import ID


class ContextID(ID):
    pass


Context = TypedDict('Context', {'id': ContextID})

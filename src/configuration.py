from typing import Any, Dict, List, Optional

from .models import Limits


class Configuration:

    def __init__(self, settings: Any):
        self.api_specification: Optional[str] = settings.api_specification
        self.aggregate_vulnerabilities: bool = settings.aggregate_vulnerabilities
        self.max_urls_per_vulnerability: int = settings.max_urls_per_vulnerability
        self.auth_auto: Optional[bool] = settings.auth_auto
        self.exclude_urls: List[str] = settings.exclude_urls
        self.auth_first_submit_field: Optional[str] = settings.auth_first_submit_field
        self.auth_password: Optional[str] = settings.auth_password
        self.auth_password_field: Optional[str] = settings.auth_password_field
        self.auth_submit_field: Optional[str] = settings.auth_submit_field
        self.auth_url: Optional[str] = settings.auth_url
        self.auth_username: Optional[str] = settings.auth_username
        self.auth_username_field: Optional[str] = settings.auth_username_field
        self.auth_verification_url: str = settings.auth_verification_url
        self.auth_verification_selector: str = settings.browserker_auth_verification_selector
        self.auth_verification_login_form: str = settings.browserker_auth_verification_login_form
        self.auth_report: bool = settings.browserker_auth_report
        self.auto_update_addons: bool = settings.auto_update_addons
        self.availability_timeout: int = settings.availability_timeout
        self.browserker_allowed_hosts: List[str] = settings.browserker_all_allowed_hosts
        self.browserker_cookies: Dict[str, str] = settings.browserker_cookies
        self.browserker_excluded_hosts: List[str] = settings.browserker_excluded_hosts
        self.browserker_ignored_hosts: List[str] = settings.browserker_ignored_hosts
        self.browserker_log: Dict[str, str] = settings.browserker_log
        self.browserker_excluded_elements: List[str] = settings.browserker_excluded_elements
        self.browserker_max_actions: int = settings.browserker_max_actions
        self.browserker_max_attack_failures: int = settings.browserker_max_attack_failures
        self.browserker_max_depth: int = settings.browserker_max_depth
        self.browserker_number_of_browsers: int = settings.browserker_number_of_browsers
        self.browserker_scan: bool = settings.browserker_scan
        self.exclude_rules: List[str] = settings.exclude_rules
        self.full_scan: Optional[bool] = settings.full_scan
        self.http_headers_to_mask: Optional[List[str]] = settings.http_headers_to_mask
        self.is_api_scan: bool = settings.is_api_scan
        self.write_addons_to_update_file: Optional[bool] = settings.write_addons_to_update_file
        self.passive_scan_max_wait_time: int = settings.passive_scan_max_wait_time
        self.paths_to_scan_list: List[str] = settings.paths_to_scan_list
        self.paths_to_scan_file_path: str = settings.paths_to_scan_file_path
        self.request_headers: Dict[str, str] = settings.request_headers
        self.script_dirs: List[str] = settings.script_dirs
        self.silent: bool = settings.silent
        self.skip_target_check: bool = settings.skip_target_check
        self.spider_mins: int = settings.spider_mins
        self.spider_start_at_host: bool = settings.spider_start_at_host
        self.target: str = settings.target
        self.urls_to_scan: List[str] = settings.urls_to_scan
        self.zap_api_host_override: Optional[str] = settings.zap_api_host_override
        self.zap_connect_sleep_seconds: Optional[int] = settings.zap_connect_sleep_seconds
        self.zap_debug: Optional[bool] = settings.zap_debug
        self.zap_default_info: Optional[str] = settings.zap_default_info
        self.zap_include_alpha: Optional[bool] = settings.zap_include_alpha
        self.zap_log_configuration: Optional[str] = settings.zap_log_configuration
        self.zap_max_connection_attempts: int = settings.zap_max_connection_attempts
        self.zap_min_level: Optional[str] = settings.zap_min_level
        self.zap_no_fail_on_warn: Optional[str] = settings.zap_no_fail_on_warn
        self.zap_other_options: Optional[str] = settings.zap_other_options
        self.zap_port: int = settings.zap_port
        self.zap_report_html: Optional[str] = settings.zap_report_html
        self.zap_report_md: Optional[str] = settings.zap_report_md
        self.zap_report_xml: Optional[str] = settings.zap_report_xml
        self.zap_use_ajax_spider: Optional[bool] = settings.zap_use_ajax_spider

        self.security_report_character_limits = Limits(summary=20000)

        self.dast_major_version: int = settings.dast_major_version
        if settings.dast_major_version < 2:
            self.full_scan_domain_validation_required: Optional[bool] = settings.full_scan_domain_validation_required
            self.zap_context_file: Optional[str] = settings.zap_context_file
            self.zap_short_format: Optional[bool] = settings.zap_short_format
            self.zap_progress_file: Optional[str] = settings.zap_progress_file
            self.zap_delay_in_seconds: Optional[str] = settings.zap_delay_in_seconds
            self.auth_display: Optional[bool] = settings.auth_display

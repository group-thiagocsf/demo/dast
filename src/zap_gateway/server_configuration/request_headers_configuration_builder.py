from src.configuration import Configuration


class RequestHeadersConfigurationBuilder:

    def __init__(self, config: Configuration):
        self._config = config

    def build(self) -> str:
        rules = []

        for index, (name, value) in enumerate((self._config.request_headers or {}).items()):
            rules.extend([
                '-config', f'replacer.full_list({index}).description=header_{index}',
                '-config', f'replacer.full_list({index}).enabled=true',
                '-config', f'replacer.full_list({index}).matchtype=REQ_HEADER',
                '-config', f'replacer.full_list({index}).matchstr={name}',
                '-config', f'replacer.full_list({index}).regex=false',
                '-config', f'replacer.full_list({index}).replacement={value}',
            ])

        return rules

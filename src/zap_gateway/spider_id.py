from typing import NewType

SpiderID = NewType('SpiderID', str)

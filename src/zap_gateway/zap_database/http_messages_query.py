from datetime import datetime
from re import match
from typing import List, Optional, Tuple

from src.models import Target
from src.models.http import HttpMessage, HttpMessages, HttpRequest, HttpResponse
from .http_status_parser import HttpStatusParser
from .zap_database import ZAPDatabase
from ..http_headers_parser import HttpHeadersParser


class HttpMessagesQuery:

    COLUMNS = ['historyid', 'method', 'reqheader', 'resheader', 'timesentmillis', 'uri']

    def __init__(self, database: ZAPDatabase, target: Target):
        self._database = database
        self._target = target

    def select(self, field_name: str, values: List[str]) -> HttpMessages:
        where_values = ','.join(values)
        results = self._database.select_many(
            f'SELECT {",".join(self.COLUMNS)} FROM history where {field_name} IN ({where_values})',
        )

        return self._parse(results)

    def _parse(self, results: List[Tuple[str, ...]]) -> HttpMessages:
        messages = []

        for result in results:
            message = self._parse_message(result)

            if message:
                messages.append(message)

        return HttpMessages(messages)

    def _parse_message(self, result: Optional[Tuple[str, ...]]) -> Optional[HttpMessage]:
        if not result or len(result) < len(self.COLUMNS) or self._is_external_resource(result):
            return None

        time_sent = self._parse_timesentmillis(result)
        status_code, status_text = HttpStatusParser().parse(result[self.COLUMNS.index('resheader')])
        request = HttpRequest(
            result[self.COLUMNS.index('method')],
            result[self.COLUMNS.index('uri')],
            HttpHeadersParser().parse(result[self.COLUMNS.index('reqheader')]),
        )
        response = HttpResponse(
            status_code, status_text, HttpHeadersParser().parse(result[self.COLUMNS.index('reqheader')]),
        )

        return HttpMessage(result[self.COLUMNS.index('historyid')], request, response, time_sent)

    def _parse_timesentmillis(self, result: Tuple[str, ...]) -> datetime:
        return datetime.utcfromtimestamp(
            self._convert_to_seconds(result[self.COLUMNS.index('timesentmillis')]),
        )

    def _convert_to_seconds(self, microseconds: str) -> float:
        return float(microseconds) / 1000.0

    def _is_external_resource(self, result: Tuple[str, ...]) -> bool:
        uri = result[self.COLUMNS.index('uri')]

        return bool(match(self._target.external_css(), uri) or match(self._target.external_js(), uri))

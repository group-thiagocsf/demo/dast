from contextlib import contextmanager

from jaydebeapi import Connection


@contextmanager
def new_cursor(connection: Connection):
    cursor = connection.cursor()

    try:
        yield cursor
    finally:
        cursor.close()

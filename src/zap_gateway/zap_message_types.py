TYPE_MANUAL = '1'               # Used for when ZAP is used as a proxy server
TYPE_SPIDER = '2'
TYPE_SPIDER_AJAX = '10'
TYPE_SPIDER_TASK = '9'
TYPE_ZAP_USER = '15'            # Used for URLs parsed in an API specification

ZAP_MESSAGE_TYPES = [TYPE_MANUAL, TYPE_SPIDER, TYPE_SPIDER_AJAX, TYPE_SPIDER_TASK, TYPE_ZAP_USER]

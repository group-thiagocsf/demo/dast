from typing import Any, List

from src.configuration import Configuration
from src.models import Rule, Rules


class RulesParser:

    def __init__(self, config: Configuration):
        self._config = config

    def parse(self, results: List[Any]) -> Rules:
        rules = []

        for result in results:
            if type(result) != dict:
                continue

            rule = Rule(result.get('id'),
                        self.is_enabled(result),
                        result.get('name'))

            rules.append(rule)

        return Rules(rules)

    def is_enabled(self, result: Any) -> bool:
        if result.get('id') in self._config.exclude_rules:
            return False

        return True if result.get('enabled') == 'true' else False

#!/usr/bin/env bash

set -e

SCRIPTS_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/script-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/changelog-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/release-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/docker-utils.sh"

log "Initializing environment"
verify_has_value "$GITLAB_API_TOKEN" "Aborting, environment variable GITLAB_API_TOKEN must contain a GitLab private token with access to this project."

PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/dast"}
VERSION="$(changelog_last_version)"
MAJOR=$(echo "$VERSION" | awk -F '.' '{print substr($1, 2)}')
MINOR=$(echo "$VERSION" | awk -F '.' '{print $2}')
PATCH=$(echo "$VERSION" | awk -F '.' '{print $3}')
CHANGELOG_DESCRIPTION=$(changelog_last_description)
RELEASE_DATA=$(build_release_json_payload "$VERSION" "$CHANGELOG_DESCRIPTION" "$PROJECT_URL")

log "Detected Secure Report Format $VERSION, verifying not already released"
verify_version_not_released "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION"

log "Releasing Docker images dast:$MAJOR.$MINOR.$PATCH to $CI_REGISTRY_IMAGE"
push_dast_to_registry "gitlab-ci-token" "$CI_JOB_TOKEN" "$CI_REGISTRY" "$CI_REGISTRY_IMAGE" "$MAJOR" "$MINOR" "$PATCH"

if [[ -n "$ALTERNATE_CI_REGISTRY_USERNAME$ALTERNATE_CI_REGISTRY_PASSWORD$ALTERNATE_CI_REGISTRY_IMAGE" ]]; then
  log "Releasing Docker images dast:$MAJOR.$MINOR.$PATCH to $ALTERNATE_CI_REGISTRY_IMAGE"
  push_dast_to_registry "$ALTERNATE_CI_REGISTRY_USERNAME" "$ALTERNATE_CI_REGISTRY_PASSWORD" "$CI_REGISTRY" "$ALTERNATE_CI_REGISTRY_IMAGE" "$MAJOR" "$MINOR" "$PATCH"
fi

log "Tagging Git SHA $CI_COMMIT_SHA with $VERSION"
tag_git_commit "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$VERSION" "$CI_COMMIT_SHA"

log "Creating GitLab release from Git tag $VERSION"
create_gitlab_release "$GITLAB_API_TOKEN" "$CI_PROJECT_ID" "$RELEASE_DATA"

#!/usr/bin/env bash

set -e

PROJECT_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)/..")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/script-utils.sh"

# create_zap_addons_issue_payload creates a payload that can be used to create a new GitLab issue.
# arguments: [New Addon Versions]
create_zap_addons_issue_payload() {
  local new_addon_versions="$1"

  verify_has_value "$new_addon_versions" "Aborting, new ZAP addon versions have not been supplied to ${FUNCNAME[0]}"

  local description="\
### Problem to solve\n\n\
ZAP has released updated versions of their addons. DAST should update the pinned addon versions in the Dockerfile to \
incorporate these changes.\n\
\n\
### Addons to update\n\
This list may be out of date; please check the latest \`zap-addons-up-to-date\` CI job to see the latest addon versions.\n"

  while IFS= read -r line; do
      description="$description- ${line//$'\n'/}\n"
  done <<< "$new_addon_versions"

  description="$description\n\
### References\n\n\
https://github.com/zaproxy/zap-extensions/releases\n\
"

  local payload="{\
    \"title\": \"Update ZAP Addons\",\
    \"labels\": \"devops::secure,group::dynamic analysis,dast::update addons\",\
    \"description\": \"$description\"\
  }"

  echo "$payload"
}

# zap_addons_issue_exists determines if there is already an open ZAP update issue
# arguments: [GitLab API token] [CI project ID] [New Addon Versions]
zap_addons_issue_exists() {
  local gitlab_token="$1"
  local project_id="$2"

  verify_has_value "$gitlab_token" "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$project_id" "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"

  local result
  local find_issue_url="https://gitlab.com/api/v4/projects/$project_id/issues?labels=devops::secure,dast::update%20addons&state=opened"

  result=$(curl --silent --fail --show-error --header "private-token:$gitlab_token" "$find_issue_url")

  if [[ "$(echo "$result" | jq '. | length')" != "0" ]]; then
    echo "Issue titled '$(echo "$result" | jq -r '.[0].title')' at $(echo "$result" | jq -r '.[0].web_url')"
  fi
}

# create_zap_addons_issue creates a new issue to update ZAP Addons if the issue is not already present.
# arguments: [GitLab API token] [CI project ID] [New Addon Versions]
create_zap_addons_issue() {
  local gitlab_token="$1"
  local project_id="$2"
  local new_addon_versions="$3"

  verify_has_value "$gitlab_token" "Aborting, GitLab CI Token has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$project_id" "Aborting, CI Project ID has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$new_addon_versions" "Aborting, new ZAP addon versions have not been supplied to ${FUNCNAME[0]}"

  local result
  local issues_url="https://gitlab.com/api/v4/projects/$project_id/issues"

  result=$(curl --silent --show-error --request POST \
                --header "PRIVATE-TOKEN:$gitlab_token" \
                --header 'Content-Type:application/json' \
                --data "$(create_zap_addons_issue_payload "$new_addon_versions")" \
                "$issues_url")

  if echo "$result" | jq -e '.id' >/dev/null; then
      echo "Update ZAP addons issue created at $(echo "$result" | jq -r '.web_url')"
  else
      echo "Failed to create issue: response was $result"
  fi
}

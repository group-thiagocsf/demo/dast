#!/bin/bash

url="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/registry/repositories?per_page=100"

total=$(curl -i --request GET --header 'Content-Type: application/json;charset=UTF-8' --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$url" | tr -d '\r' | grep 'x-total-pages' | sed 's/.*\([^0-9]\)/\1/')

folder=$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA

for ((i=1; i<=total; i++))
do
  id=$(curl --request GET --header 'Content-Type: application/json;charset=UTF-8' --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$url&page=$i" | jq --arg folder "$folder" '.[] | select(.name==$folder)["id"]')

  if [ "$id" != "null" ]
  then
    break
  fi
done


if [ "$id" != "null" ]
then
  delete_url="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/registry/repositories/$id"
  echo "DELETING $delete_url"
  curl --request DELETE --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "$delete_url"
else
  echo "Couldn't find $folder"
fi

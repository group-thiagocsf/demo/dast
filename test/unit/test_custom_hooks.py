import json
import unittest
from collections import namedtuple
from unittest import mock
from unittest.mock import DEFAULT, MagicMock, Mock, call, mock_open, patch

from src.custom_hooks import CustomHooks
from src.models.errors import InvalidStateError
from src.models.http import HttpMessages
from src.services import PythonObjectEncoder
from test.unit import factories
from test.unit.factories.models import http as http_factories
from test.unit.mock_config import ToConfig


class TestCustomHooks(unittest.TestCase):

    def setUp(self):
        self.num_records_to_scan = []
        self.scan_data = {'id': 0, 'progress': '100', 'state': 'FINISHED'}
        self.full_result_data = [{'urlsInScope': []},
                                 {'urlsOutOfScope': []},
                                 {'urlsIoError': []}]

        self.script_finder = MagicMock()
        self.zaproxy = MagicMock()
        self.zaproxy.remaining_records_to_passive_scan = MagicMock(return_value=0)
        self.zaproxy.scanned_resources = MagicMock(return_value=HttpMessages())
        self.zaproxy.spider_scans = MagicMock(return_value=[self.scan_data])
        self.zaproxy.spider.full_results = MagicMock(return_value=self.full_result_data)
        self.zap_daemon = MagicMock()
        self.zap_client = MagicMock()

        self.system = MagicMock()
        self.config = ToConfig(
            exclude_rules=['14007'],
            is_api_scan=False,
            passive_scan_max_wait_time=1,
            target='some_target',
            zap_report_html='report.html',
            zap_report_xml='report.xml',
            zap_report_md='report.md',
        )
        self.dast_report = {'problems': 'many'}
        self.report_formatter = namedtuple('rpt_formatter', 'format_report')(
            format_report=MagicMock(return_value=self.dast_report),
        )
        self.hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, self.config, self.script_finder)

    def test_handover_to_dast_should_load_scripts(self):
        self.script_finder.find_scripts.return_value = [factories.models.script(name='script-a.js'),
                                                        factories.models.script(name='script-b.js')]

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ):
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        self.assertEqual(2, len(self.zaproxy.load_script.mock_calls))

    def test_handover_to_dast_should_exclude_rules(self):
        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            open=DEFAULT,
        ):
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        self.zaproxy.exclude_rules.assert_called_once_with(['14007'])

    def test_handover_to_dast_no_scans(self):
        self.zaproxy.spider_scans = MagicMock(return_value=[])

        with self.assertRaises(InvalidStateError), \
             patch.multiple(
                'src.custom_hooks',
                Spider=DEFAULT,
                TargetSelector=DEFAULT,
                open=DEFAULT,
        ):
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

    def test_handover_to_dast_add_scanned_urls_to_report(self):
        self.zaproxy.scanned_resources = MagicMock(return_value=HttpMessages([
            http_factories.http_message(),
            http_factories.http_message(),
        ]))

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            json=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_json = mock_dependencies['json']
        mock_open = mock_dependencies['open']

        mock_json.dump.assert_called_with(
            {'problems': 'many'},
            mock_open.return_value.__enter__.return_value,
            cls=PythonObjectEncoder,
        )

    def test_handover_to_dast_log_scanned_urls(self):
        self.zaproxy.scanned_resources = MagicMock(return_value=HttpMessages([
            http_factories.http_message(request=http_factories.http_request(method='GET',
                                                                            url='http://nginx/')),
            http_factories.http_message(request=http_factories.http_request(method='GET',
                                                                            url='http://nginx/robots.txt')),
        ]))

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            json=DEFAULT,
            logging=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_logging = mock_dependencies['logging']

        expected_log = """The following 2 URLs were scanned:\nGET http://nginx/\nGET http://nginx/robots.txt"""
        mock_logging.info.assert_called_with(expected_log)

    def test_handover_to_dast_log_scanner_urls_when_no_urls_were_scanned(self):
        self.zaproxy.scanned_resources = MagicMock(return_value=HttpMessages())

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            json=DEFAULT,
            logging=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_logging = mock_dependencies['logging']

        mock_logging.info.assert_called_with('0 URLs were scanned.')

    def test_handover_to_dast_writes_zap_reports(self):
        write_html_report = MagicMock()
        write_xml_report = MagicMock()
        write_md_report = MagicMock()
        self.zaproxy.scanned_resources = MagicMock(return_value=HttpMessages())
        self.zaproxy.write_html_report = write_html_report
        self.zaproxy.write_xml_report = write_xml_report
        self.zaproxy.write_md_report = write_md_report

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ):
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        write_html_report.assert_called_once_with('report.html')
        write_xml_report.assert_called_once_with('report.xml')
        write_md_report.assert_called_once_with('report.md')

    def test_handover_to_dast_prints_rules_and_summary(self):
        rules = MagicMock()
        alerts = MagicMock()
        self.zaproxy.alerts = MagicMock(return_value=alerts)
        self.zaproxy.executed_rules = MagicMock(return_value=rules)
        self.zaproxy.scanned_resources = MagicMock(return_value=HttpMessages())

        with patch.multiple(
            'src.custom_hooks',
            ActiveScan=DEFAULT,
            ScanSummaryService=DEFAULT,
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            mock_active_scan = mock_dependencies['ActiveScan']
            mock_active_scan.return_value.policy_name = 'Default Policy'

            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_printer = mock_dependencies['ScanSummaryService']

        self.zaproxy.executed_rules.assert_called_once_with('Default Policy')
        mock_printer.assert_called_once_with(rules, alerts, self.system)

        mock_printer_instance = mock_printer.return_value
        mock_printer_instance.print_results.assert_called_once()

    def test_handover_to_dast_waits_for_passive_scan(self):
        self.num_records_to_scan = [0, 3]
        self.zaproxy.remaining_records_to_passive_scan = MagicMock(side_effect=self.mock_remaining_records)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            logging=DEFAULT,
            open=DEFAULT,
            time=DEFAULT,
        ) as mock_dependencies:
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_logging = mock_dependencies['logging']
        mock_time = mock_dependencies['time']

        mock_time.sleep.assert_called_once_with(2)
        mock_logging.debug.assert_has_calls([
            call('Records to passive scan: 3'),
            call('Passive scanning complete!'),
        ])

    def test_handover_to_dast_times_out_passive_scan_if_exceeds_configured_timeout(self):
        default_wait_time = self.hooks.PASSIVE_SCAN_WAIT_SLEEP_SECONDS

        self.hooks.PASSIVE_SCAN_WAIT_SLEEP_SECONDS = 61
        self.num_records_to_scan = [0, 3]
        self.zaproxy.remaining_records_to_passive_scan = MagicMock(side_effect=self.mock_remaining_records)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            logging=DEFAULT,
            open=DEFAULT,
            time=DEFAULT,
        ) as mock_dependencies:
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        self.hooks.PASSIVE_SCAN_WAIT_SLEEP_SECONDS = default_wait_time

        mock_logging = mock_dependencies['logging']
        mock_time = mock_dependencies['time']

        mock_time.sleep.assert_called_once_with(61)
        mock_logging.debug.assert_has_calls([
            call('Records to passive scan: 3'),
            call('Exceeded passive scan timeout of 60s'),
        ])

    def test_handover_to_dast_runs_active_scan_if_full_scan(self):
        config = ToConfig(full_scan=True, passive_scan_max_wait_time=1, is_api_scan=False, target='not_url')
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            ActiveScan=DEFAULT,
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            mock_target_selector = mock_dependencies['TargetSelector']
            mock_target = Mock()
            mock_target_selector.return_value.select.return_value = mock_target

            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_active_scan = mock_dependencies['ActiveScan']
        mock_target_selector = mock_dependencies['TargetSelector']

        mock_target_selector.assert_called_once_with(self.zaproxy, config)
        mock_target_selector.return_value.select.assert_called_once()
        mock_active_scan.assert_called_once_with(self.zaproxy, mock_target, False)
        mock_active_scan.return_value.run.assert_called_once()

    def test_handover_to_dast_does_not_run_active_scan_if_not_full_scan(self):
        with patch.multiple(
            'src.custom_hooks',
            ActiveScan=DEFAULT,
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_active_scan = mock_dependencies['ActiveScan']

        mock_active_scan.return_value.run.assert_not_called()

    def test_handover_to_dast_runs_ajax_spider_if_configured(self):
        config = ToConfig(zap_use_ajax_spider=True, spider_mins=666, passive_scan_max_wait_time=1, target='not_url')
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            AJAXSpider=DEFAULT,
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            mock_target_selector = mock_dependencies['TargetSelector']
            mock_target = Mock()
            mock_target_selector.return_value.select.return_value = mock_target

            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_ajax_spider = mock_dependencies['AJAXSpider']
        mock_target_selector = mock_dependencies['TargetSelector']

        mock_target_selector.assert_called_once_with(self.zaproxy, config)
        mock_target_selector.return_value.select.assert_called_once()
        mock_ajax_spider.assert_called_once_with(self.zaproxy, mock_target, 666)
        mock_ajax_spider.return_value.run.assert_called_once()

    def test_handover_to_dast_does_not_run_ajax_spider_if_not_configured(self):
        with patch.multiple(
            'src.custom_hooks',
            AJAXSpider=DEFAULT,
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_ajax_spider = mock_dependencies['AJAXSpider']

        mock_ajax_spider.assert_not_called()

    def test_handover_to_dast_never_runs_ajax_spider_if_api_scan(self):
        config = ToConfig(
            is_api_scan=True,
            zap_use_ajax_spider=True,
            spider_mins=666,
            passive_scan_max_wait_time=1,
            target='not_url',
        )
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            open=DEFAULT,
            AJAXSpider=DEFAULT,
            TargetSelector=DEFAULT,
        ) as mock_dependencies:
            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_ajax_spider = mock_dependencies['AJAXSpider']

        mock_ajax_spider.assert_not_called()

    def test_handover_to_dast_runs_url_scan_if_urls_to_scan(self):
        urls_to_scan = 'http://website/1,http://website/2'
        config = ToConfig(urls_to_scan=urls_to_scan, passive_scan_max_wait_time=1, exclude_urls=[])
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            TargetSelector=DEFAULT,
            URLScan=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            mock_target_selector = mock_dependencies['TargetSelector']
            mock_target = Mock()
            mock_target_selector.return_value.select.return_value = mock_target

            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_url_scan = mock_dependencies['URLScan']

        mock_url_scan.assert_called_once_with(self.zaproxy, mock_target, urls_to_scan, [])
        mock_url_scan.return_value.run.assert_called_once()

    def test_handover_to_dast_does_not_runs_url_scan_if_urls_to_scan_not_set(self):
        urls_to_scan = None
        config = ToConfig(urls_to_scan=urls_to_scan, passive_scan_max_wait_time=1, target='not_url')
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            URLScan=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_url_scan = mock_dependencies['URLScan']
        mock_url_scan.assert_not_called()

    def test_handover_to_dast_runs_url_scan_and_not_spider(self):
        urls_to_scan = 'http://website/1,http://website/2'
        config = ToConfig(
            zap_use_ajax_spider=True, urls_to_scan=urls_to_scan, passive_scan_max_wait_time=1, exclude_urls=[])
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            AJAXSpider=DEFAULT,
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            URLScan=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            mock_target_selector = mock_dependencies['TargetSelector']
            mock_target = Mock()
            mock_target_selector.return_value.select.return_value = mock_target

            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_ajax_spider = mock_dependencies['AJAXSpider']
        mock_spider = mock_dependencies['Spider']
        mock_url_scan = mock_dependencies['URLScan']

        mock_url_scan.assert_called_once_with(self.zaproxy, mock_target, urls_to_scan, [])
        mock_url_scan.return_value.run.assert_called_once()
        mock_ajax_spider.assert_not_called()
        mock_spider.assert_not_called()

    def test_handover_to_dast_runs_spider(self):
        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            mock_target_selector = mock_dependencies['TargetSelector']
            mock_target = Mock()
            mock_target_selector.return_value.select.return_value = mock_target

            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_spider = mock_dependencies['Spider']
        mock_target_selector = mock_dependencies['TargetSelector']

        mock_target_selector.assert_called_once_with(self.zaproxy, self.config)
        mock_target_selector.return_value.select.assert_called_once()
        mock_spider.assert_called_once_with(self.zaproxy, mock_target)
        mock_spider.return_value.run.assert_called_once()

    def test_handover_to_dast_does_not_run_spider_if_api_scan(self):
        config = ToConfig(is_api_scan=True, passive_scan_max_wait_time=1, target='not_url')
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_spider = mock_dependencies['Spider']

        mock_spider.assert_not_called()

    def test_handover_to_dast_verifies_target_when_not_url_scan_and_target_is_url(self):
        config = ToConfig(passive_scan_max_wait_time=1, target='http://test.site/path')
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetAvailability=DEFAULT,
            TargetProbe=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_target_accessibility = mock_dependencies['TargetAvailability']

        mock_target_accessibility.assert_called_once_with('http://test.site/path', config)
        mock_target_accessibility.return_value.verify.assert_called_once()

    def test_handover_to_dast_sends_probe_with_proxy(self):
        config = ToConfig(passive_scan_max_wait_time=1, urls_to_scan=[], target='http://vulnerable_website')
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetAvailability=DEFAULT,
            TargetProbe=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            self.zap_daemon.proxy_endpoint.return_value = 'proxy_endpoint'

            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_target_probe = mock_dependencies['TargetProbe']

        mock_target_probe.assert_called_once_with('http://vulnerable_website', config, proxy='proxy_endpoint')
        mock_target_probe.return_value.send_ping.assert_called_once()

    def test_handover_to_dast_does_not_sends_probe_with_proxy_for_browserker_scans(self):
        config = ToConfig(
            passive_scan_max_wait_time=1, urls_to_scan=[], target='http://vulnerable_website', browserker_scan=True)
        hooks = CustomHooks(
            self.zaproxy, self.report_formatter, self.system,
            config, self.script_finder,
        )

        with patch.multiple(
            'src.custom_hooks',
            TargetAvailability=DEFAULT,
            TargetProbe=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
            BrowserkerScan=DEFAULT,
            URLScan=DEFAULT,
        ) as mock_dependencies:
            self.zap_daemon.proxy_endpoint.return_value = 'proxy_endpoint'

            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_target_probe = mock_dependencies['TargetProbe']

        mock_target_probe.assert_not_called()

    def test_handover_to_dast_authenticates_target_if_configured(self):
        zap_daemon = Mock()
        zap_daemon.proxy_endpoint.return_value = 'http://proxy'
        config = ToConfig(
            auth_url='http://vulnerable_website/login',
            availability_timeout=1,
            passive_scan_max_wait_time=1,
            target='http://vulnerable_website',
        )
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetAuthenticator=DEFAULT,
            TargetAvailability=DEFAULT,
            TargetProbe=DEFAULT,
            TargetSelector=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            mock_selector = mock_dependencies['TargetSelector']
            mock_selector.return_value.select.return_value = 'http://vulnerable_website'

            hooks.handover_to_dast(self.zap_client, zap_daemon)

        mock_target_auth = mock_dependencies['TargetAuthenticator']
        mock_target_auth.assert_called_once_with(
            'http://vulnerable_website', self.zap_client, config, 'http://proxy',
        )
        mock_target_auth.return_value.authenticate.assert_called_once()

    def test_handover_to_dast_skips_target_check_if_asked(self):
        config = ToConfig(
            passive_scan_max_wait_time=1,
            skip_target_check=True,
            urls_to_scan=[],
            target='http://vulnerable_website',
        )
        hooks = CustomHooks(self.zaproxy, self.report_formatter, self.system, config, self.script_finder)

        with patch.multiple(
            'src.custom_hooks',
            Spider=DEFAULT,
            TargetAvailability=DEFAULT,
            TargetProbe=DEFAULT,
            open=DEFAULT,
        ) as mock_dependencies:
            hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_target_availability = mock_dependencies['TargetAvailability']
        mock_target_availability.assert_not_called()

    def test_runs_browserker_scan(self):
        self.config.browserker_scan = True

        with patch.multiple(
            'src.custom_hooks',
            BrowserkerScan=DEFAULT,
            Spider=DEFAULT,
            AJAXSpider=DEFAULT,
            URLScan=DEFAULT,
            open=DEFAULT,
            json=DEFAULT,
            TargetAuthenticator=DEFAULT,
        ) as mock_dependencies:
            mock_dependencies['BrowserkerScan'].return_value.run.return_value = []
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_dependencies['BrowserkerScan'].assert_called_once()
        mock_dependencies['BrowserkerScan'].return_value.run.assert_called_once()
        mock_dependencies['TargetAuthenticator'].assert_not_called()
        mock_dependencies['Spider'].assert_not_called()
        mock_dependencies['AJAXSpider'].assert_not_called()
        mock_dependencies['URLScan'].assert_not_called()

    def test_runs_url_scan_after_browserker_scan(self):
        self.config.browserker_scan = True

        with patch.multiple(
            'src.custom_hooks',
            BrowserkerScan=DEFAULT,
            Spider=DEFAULT,
            AJAXSpider=DEFAULT,
            URLScan=DEFAULT,
            open=DEFAULT,
            json=DEFAULT,
        ) as mock_dependencies:
            mock_dependencies['BrowserkerScan'].return_value.run.return_value = [
                'http://example.com', 'http://example.com/url_1?']
            self.zaproxy.scanned_urls_from_api = MagicMock(return_value=['http://example.com'])
            self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        mock_dependencies['BrowserkerScan'].assert_called_once()
        mock_dependencies['BrowserkerScan'].return_value.run.assert_called_once()

    def test_write_addons_to_update_file(self):
        self.config.write_addons_to_update_file = True

        example_update = [{
            'url': 'https://example.com/spider-release-23.2.0.zap',
            'name': 'Spider',
        }]

        self.zaproxy.updated_addons = MagicMock(return_value=json.dumps(example_update))

        with patch.multiple(
            'src.custom_hooks',
            BrowserkerScan=DEFAULT,
            Spider=DEFAULT,
            AJAXSpider=DEFAULT,
            URLScan=DEFAULT,
        ) as mock_dependencies:
            with patch('src.custom_hooks.open', mock_open()) as m:
                self.hooks.handover_to_dast(self.zap_client, self.zap_daemon)

        m.assert_called_once_with('/zap/wrk/addons.json', 'w')
        handle = m()
        handle.write.assert_has_calls([
            mock.call('"[{\\"url\\": \\"https://example.com/spider-release-23.2.0.zap\\", \\"name\\": \\"Spider\\"}]"'),
        ])

        mock_dependencies['BrowserkerScan'].assert_not_called()
        mock_dependencies['Spider'].assert_not_called()
        mock_dependencies['AJAXSpider'].assert_not_called()
        mock_dependencies['URLScan'].assert_not_called()

        self.zap_daemon.shutdown.assert_called_once()
        self.system.sys_exit.assert_called_once_with(0)

    def mock_remaining_records(self, *args, **kwargs):
        return self.num_records_to_scan.pop()

import unittest
from unittest.mock import MagicMock, patch

from src.system import System


class TestSystem(unittest.TestCase):

    def test_should_return_free_port(self):
        with patch('src.system.socket') as socket:
            sock = MagicMock()
            sock.getsockname.return_value = ('0.0.0.0', 5001)
            sock.connect_ex.return_value = 1
            socket.socket.return_value = sock

            port = System().get_free_port()

            self.assertEqual(1, len(sock.close.mock_calls))
            self.assertEqual(port, 5001)

    def test_get_free_port_should_throw_exception_if_unable_to_connect(self):
        with patch('src.system.socket') as socket:
            sock = MagicMock()
            sock.connect_ex.return_value = 0
            socket.socket.return_value = sock

            try:
                System().get_free_port()
                self.assertTrue(False)
            except RuntimeError as e:
                self.assertIn('Failed to find free port', str(e))

    @patch('builtins.open')
    def test_should_run_a_program(self, _mock_open):
        with patch('src.system.Popen') as Popen:
            System().run(['ls', '-la'], output_file_name='log.txt', cwd='/tmp')

            self.assertEqual(1, len(Popen.mock_calls))
            self.assertEqual(['ls', '-la'], Popen.mock_calls[0][1][0])
            self.assertIsNotNone(Popen.mock_calls[0].kwargs['stdout'])
            self.assertIsNotNone(Popen.mock_calls[0].kwargs['stderr'])
            self.assertEqual('/tmp', Popen.mock_calls[0].kwargs['cwd'])

    @patch('src.system.Popen')
    def test_should_return_dast_version(self, mock_popen):
        mock_popen.return_value.communicate.return_value = (b'v1.27.0\n', b'')
        self.assertEqual('1.27.0', System().dast_version())

    @patch('src.system.Popen')
    def test_should_empty_dast_version_when_values_are_none(self, mock_popen):
        mock_popen.return_value.communicate.return_value = (None, None)
        self.assertEqual('', System().dast_version())

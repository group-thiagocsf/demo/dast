from unittest import TestCase

from test.unit.factories.models import f_aggregated_alert, f_alert


class TestAggregatedAlert(TestCase):

    def setUp(self) -> None:
        self.alert1 = f_alert()
        self.alert2 = f_alert()
        self.aggregated_alert = f_aggregated_alert([self.alert1, self.alert2])

    def test_message_returns_message_if_present(self):
        self.assertEqual(self.aggregated_alert.message, self.alert1.message)

    def test_url_returns_url_if_present(self):
        self.assertEqual(self.aggregated_alert.url, self.alert1.url)

    def test_response_code_returns_message_response_status_code(self):
        self.assertEqual(self.aggregated_alert.response_code, self.alert1.response_code)

    def test_rule_id_returns_rule_id_if_present(self):
        self.assertEqual(self.aggregated_alert.rule_id, self.alert1.rule_id)

    def test_id_returns_alert_id(self):
        self.assertEqual(self.aggregated_alert.id, self.alert1.id)

    def test_cwe_id_returns_cwe_id_if_present(self):
        self.assertEqual(self.aggregated_alert.cwe_id, self.alert1.cwe_id)

    def test_source_id_returns_source_id_if_present(self):
        self.assertEqual(self.aggregated_alert.source_id, self.alert1.source_id)

    def test_detail_returns_detail_if_present(self):
        self.assertEqual(self.aggregated_alert.detail, self.alert1.detail)

    def test_confidence_returns_confidence_if_present(self):
        self.assertEqual(self.aggregated_alert.confidence, self.alert1.confidence)

    def test_description_returns_description_if_present(self):
        self.assertEqual(self.aggregated_alert.description, self.alert1.description)

    def test_risk_returns_risk_if_present(self):
        self.assertEqual(self.aggregated_alert.risk, self.alert1.risk)

    def test_evidence_returns_evidence_if_present(self):
        self.assertEqual(self.aggregated_alert.evidence, self.alert1.evidence)

    def test_name_returns_name_if_present(self):
        self.assertEqual(self.aggregated_alert.name, self.alert1.name)

    def test_method_returns_method_if_present(self):
        self.assertEqual(self.aggregated_alert.method, self.alert1.method)

    def test_param_returns_param_if_present(self):
        self.assertEqual(self.aggregated_alert.param, self.alert1.param)

    def test_reference_returns_reference_if_present(self):
        self.assertEqual(self.aggregated_alert.reference, self.alert1.reference)

    def test_solution_returns_solution_if_present(self):
        self.assertEqual(self.aggregated_alert.solution, self.alert1.solution)

    def test_attack_returns_attack_if_present(self):
        self.assertEqual(self.aggregated_alert.attack, self.alert1.attack)

    def test_is_an_aggregated_alert(self):
        self.assertTrue(self.aggregated_alert.is_aggregated)

    def test_aggregated_alerts_are_returned(self):
        self.assertEqual(self.aggregated_alert.aggregated_alerts(), [self.alert1, self.alert2])

from unittest import TestCase

from src.models import Alert, AlertID, CweID, RuleID, SourceID
from test.unit.factories.models.http import http_message as f_http_message, \
                                            http_response as f_http_response
from test.unit.factories.zap import alert as f_zap_alert


class TestAlert(TestCase):

    def test_message_returns_message_if_present(self):
        raw_alert = {'message': 'test-message'}

        alert = Alert(raw_alert)

        self.assertEqual(alert.message, 'test-message')

    def test_message_returns_none_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['message']

        alert = Alert(zap_alert)

        self.assertEqual(alert.message, None)

    def test_url_returns_url_if_present(self):
        raw_alert = {'url': 'https://en.wikipedia.org/wiki/Sylvia_Rivera'}

        alert = Alert(raw_alert)

        self.assertEqual(alert.url, 'https://en.wikipedia.org/wiki/Sylvia_Rivera')

    def test_url_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['url']

        alert = Alert(zap_alert)

        self.assertEqual(alert.url, '')

    def test_response_code_returns_message_response_status_code(self):
        response = f_http_response(status=420)
        message = f_http_message(response=response)
        parsed_alert = f_zap_alert(message=message)

        alert = Alert(parsed_alert)

        self.assertEqual(alert.response_code, 420)

    def test_response_code_is_none_if_there_is_no_message(self):
        zap_alert = f_zap_alert()
        del zap_alert['message']
        alert = Alert(zap_alert)

        self.assertIsNone(alert.response_code)

    def test_rule_id_returns_rule_id_if_present(self):
        parsed_alert = {'pluginId': 'test-id'}

        alert = Alert(parsed_alert)

        self.assertEqual(alert.rule_id, RuleID('test-id'))

    def test_rule_id_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['pluginId']

        alert = Alert(zap_alert)

        self.assertEqual(alert.rule_id, RuleID(''))

    def test_id_returns_alert_id(self):
        zap_alert = f_zap_alert(alert_id='666')

        alert = Alert(zap_alert)

        self.assertEqual(alert.id, AlertID('666'))

    def test_cwe_id_returns_cwe_id_if_present(self):
        zap_alert = f_zap_alert(cwe_id='666')

        alert = Alert(zap_alert)

        self.assertEqual(alert.cwe_id, CweID('666'))

    def test_cwe_id_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['cweid']

        alert = Alert(zap_alert)

        self.assertEqual(alert.cwe_id, CweID(''))

    def test_source_id_returns_source_id_if_present(self):
        zap_alert = f_zap_alert(source_id='666')

        alert = Alert(zap_alert)

        self.assertEqual(alert.source_id, SourceID('666'))

    def test_source_id_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['sourceid']

        alert = Alert(zap_alert)

        self.assertEqual(alert.source_id, SourceID(''))

    def test_detail_returns_detail_if_present(self):
        zap_alert = f_zap_alert(other='mas detalles')

        alert = Alert(zap_alert)

        self.assertEqual(alert.detail, 'mas detalles')

    def test_detail_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['other']

        alert = Alert(zap_alert)

        self.assertEqual(alert.detail, '')

    def test_confidence_returns_confidence_if_present(self):
        zap_alert = f_zap_alert(confidence='super low')

        alert = Alert(zap_alert)

        self.assertEqual(alert.confidence, 'super low')

    def test_confidence_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['confidence']

        alert = Alert(zap_alert)

        self.assertEqual(alert.confidence, '')

    def test_description_returns_description_if_present(self):
        zap_alert = f_zap_alert(description='oops a vulnerability')

        alert = Alert(zap_alert)

        self.assertEqual(alert.description, 'oops a vulnerability')

    def test_description_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['description']

        alert = Alert(zap_alert)

        self.assertEqual(alert.description, '')

    def test_risk_returns_risk_if_present(self):
        zap_alert = f_zap_alert(risk='snoop')

        alert = Alert(zap_alert)

        self.assertEqual(alert.risk, 'snoop')

    def test_risk_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['risk']

        alert = Alert(zap_alert)

        self.assertEqual(alert.risk, '')

    def test_riskcode_returns_the_code_for_the_risk(self):
        zap_alert = f_zap_alert(risk='Low')

        alert = Alert(zap_alert)

        self.assertEqual(alert.riskcode, '1')

    def test_riskcode_returns_empty_string_when_the_risk_is_not_recognised(self):
        zap_alert = f_zap_alert(risk='foo')

        alert = Alert(zap_alert)

        self.assertEqual(alert.riskcode, '')

    def test_riskcode_returns_0_if_no_risk_is_set(self):
        zap_alert = f_zap_alert()
        del zap_alert['risk']

        alert = Alert(zap_alert)

        self.assertEqual(alert.riskcode, '0')

    def test_evidence_returns_evidence_if_present(self):
        zap_alert = f_zap_alert(evidence='lol')

        alert = Alert(zap_alert)

        self.assertEqual(alert.evidence, 'lol')

    def test_evidence_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['evidence']

        alert = Alert(zap_alert)

        self.assertEqual(alert.evidence, '')

    def test_name_returns_name_if_present(self):
        zap_alert = f_zap_alert(name='injection')

        alert = Alert(zap_alert)

        self.assertEqual(alert.name, 'injection')

    def test_name_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['name']

        alert = Alert(zap_alert)

        self.assertEqual(alert.name, '')

    def test_method_returns_method_if_present(self):
        zap_alert = f_zap_alert(method='DELETE')

        alert = Alert(zap_alert)

        self.assertEqual(alert.method, 'DELETE')

    def test_method_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['method']

        alert = Alert(zap_alert)

        self.assertEqual(alert.method, '')

    def test_param_returns_param_if_present(self):
        zap_alert = f_zap_alert(param='something')

        alert = Alert(zap_alert)

        self.assertEqual(alert.param, 'something')

    def test_param_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['param']

        alert = Alert(zap_alert)

        self.assertEqual(alert.param, '')

    def test_reference_returns_reference_if_present(self):
        zap_alert = f_zap_alert(reference='http://security.test')

        alert = Alert(zap_alert)

        self.assertEqual(alert.reference, 'http://security.test')

    def test_reference_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['reference']

        alert = Alert(zap_alert)

        self.assertEqual(alert.reference, '')

    def test_solution_returns_solution_if_present(self):
        zap_alert = f_zap_alert(solution='Give up')

        alert = Alert(zap_alert)

        self.assertEqual(alert.solution, 'Give up')

    def test_solution_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['solution']

        alert = Alert(zap_alert)

        self.assertEqual(alert.solution, '')

    def test_attack_returns_attack_if_present(self):
        zap_alert = f_zap_alert(attack='lol.zap')

        alert = Alert(zap_alert)

        self.assertEqual(alert.attack, 'lol.zap')

    def test_attack_returns_empty_string_if_not_present(self):
        zap_alert = f_zap_alert()
        del zap_alert['attack']

        alert = Alert(zap_alert)

        self.assertEqual(alert.attack, '')

    def test_is_not_an_aggregated_alert(self):
        alert = Alert({'message': 'test-message'})
        self.assertFalse(alert.is_aggregated)

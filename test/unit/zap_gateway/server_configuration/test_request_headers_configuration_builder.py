from unittest import TestCase
from unittest.mock import MagicMock

from src.zap_gateway.server_configuration import RequestHeadersConfigurationBuilder


class TestRequestHeadersConfigurationBuilder(TestCase):

    def test_should_add_request_headers(self):
        config = MagicMock(request_headers={'Authorization': 'Bearer my.token'})

        configuration = RequestHeadersConfigurationBuilder(config).build()

        normalized = ' '.join(configuration)
        self.assertIn('-config replacer.full_list(0).description=header_0', normalized)
        self.assertIn('-config replacer.full_list(0).enabled=true', normalized)
        self.assertIn('-config replacer.full_list(0).matchtype=REQ_HEADER', normalized)
        self.assertIn('-config replacer.full_list(0).matchstr=Authorization', normalized)
        self.assertIn('-config replacer.full_list(0).regex=false', normalized)
        self.assertIn('-config replacer.full_list(0).replacement=Bearer my.token', normalized)

    def test_does_not_add_request_headers_when_there_are_none(self):
        config = MagicMock(request_headers={})

        configuration = RequestHeadersConfigurationBuilder(config).build()

        self.assertEqual([], configuration)

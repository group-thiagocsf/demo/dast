import unittest
from unittest.mock import MagicMock

from lib.zap.zap_addons import ZAPAddons
from test.unit import utilities


class TestZAPAddons(unittest.TestCase):

    def test_prints_addon(self):
        zap_versions = '<?xml version="1.0" encoding="UTF-8" standalone="no"?><ZAP>' \
                       '  <addon>accessControl</addon>' \
                       '  <addon_accessControl>' \
                       '    <name>Access Control Testing</name>' \
                       '    <description>access control description</description>' \
                       '    <author>ZAP Dev Team</author>' \
                       '    <version>5</version>' \
                       '    <file>accessControl-alpha-5.zap</file>' \
                       '    <status>alpha</status>' \
                       '    <changes>access control description</changes>' \
                       '    <url>https://url/accessControl-alpha-5.zap</url>' \
                       '    <hash>SHA1:8160d7a28f9952a3760299d0bc30c32982b75274</hash>' \
                       '    <info>https://www.zaproxy.org/docs/desktop/addons/access-control-testing/</info>' \
                       '    <repo>https://github.com/zaproxy/zap-extensions/</repo>' \
                       '    <date>2018-11-02</date>' \
                       '    <size>539232</size>' \
                       '    <not-before-version>2.7.0</not-before-version>' \
                       '  </addon_accessControl>' \
                       '</ZAP>'

        system = MagicMock(notify=MagicMock())

        with utilities.httpserver.new().respond(status=200, content=zap_versions).build() as server:
            ZAPAddons(system, server.address()).metadata('accessControl')

            notify_calls = [call[1][0].strip() for call in system.notify.mock_calls]

            self.assertIn('accessControl:', notify_calls)
            self.assertIn('name: Access Control Testing', notify_calls)
            self.assertIn('release page: https://url/', notify_calls)
            self.assertIn('url: https://url/accessControl-alpha-5.zap', notify_calls)

import json


def messages_har(messages=[]):
    messages_dict = {
        'log': {
            'version': '1.2',
            'creator': {
                'name': 'OWASP ZAP',
                'version': 'D-2020-02-10',
            },
            'entries': list(map(lambda message: entry_har(message), messages)),
        },
    }

    return json.dumps(messages_dict)


def entry_har(entry):
    message_type = '2'
    if 'type' in entry:
        message_type = entry['type']

    entry_dict = {
        'startedDateTime': '2020-03-04T08:09:09.992+00:00',
        'time': 10,
        'request': {
            'method': entry['method'],
            'url': entry['url'],
            'httpVersion': 'HTTP/1.1',
            'cookies': [],
            'headers': [{
                'name': 'User-Agent',
                'value': 'python-requests/2.20.1',
            }, {
                'name': 'Accept',
                'value': '*/*',
            }, {
                'name': 'Connection',
                'value': 'keep-alive',
            }, {
                'name': 'Host',
                'value': 'nginx',
            }],
            'queryString': [],
            'postData': {
                'mimeType': '',
                'params': [],
                'text': '',
            },
            'headersSize': 116,
            'bodySize': 0,
        },
        'response': {
            'status': 200,
            'statusText': 'OK',
            'httpVersion': 'HTTP/1.1',
            'cookies': [],
            'headers': [{
                'name': 'Server',
                'value': 'nginx/1.17.6',
            }, {
                'name': 'Date',
                'value': 'Wed, 04 Mar 2020 08:09:10 GMT',
            }, {
                'name': 'Content-Type',
                'value': 'text/html',
            }, {
                'name': 'Content-Length',
                'value': '277',
            }, {
                'name': 'Last-Modified',
                'value': 'Wed, 12 Feb 2020 03:55:42 GMT',
            }, {
                'name': 'Connection',
                'value': 'keep-alive',
            }, {
                'name': 'ETag',
                'value': "'5e43773e-115'",
            }, {
                'name': 'Accept-Ranges',
                'value': 'bytes',
            }],
            'content': {
                'size': 277,
                'compression': 0,
                'mimeType': 'text/html',
                'text': '',
            },
            'redirectURL': '',
            'headersSize': 238,
            'bodySize': 277,
        },
        'cache': {},
        'timings': {
            'send': 0,
            'wait': 0,
            'receive': 10,
        },
        '_zapMessageId': '1',
        '_zapMessageNote': '',
        '_zapMessageType': message_type,
    }
    if entry['url'] is None:
        entry_dict['request'].pop('url', None)

    if entry['method'] is None:
        entry_dict['request'].pop('method', None)

    return entry_dict

import json


def message_har_entry(url='http://my.site',
                      status_code=200,
                      request_headers=[{'name': 'User-Agent', 'value': 'python-requests/2.20.1'},
                                       {'name': 'Accept', 'value': '*/*'},
                                       {'name': 'Connection', 'value': 'keep-alive'},
                                       {'name': 'Host', 'value': 'nginx'}],
                      response_headers=[{'name': 'Server', 'value': 'nginx/1.17.6'},
                                        {'name': 'Date', 'value': 'Tue, 05 May 2020 06:39:43 GMT'},
                                        {'name': 'Content-Type', 'value': 'text/html'},
                                        {'name': 'Content-Length', 'value': '277'},
                                        {'name': 'Last-Modified', 'value': 'Fri, 15 Nov 2019 03:54:29 GMT'},
                                        {'name': 'Connection', 'value': 'keep-alive'},
                                        {'name': 'ETag', 'value': '"5dce2175-115"'},
                                        {'name': 'Accept-Ranges', 'value': 'bytes'}],
                      request_post_data={'mimeType': '', 'params': [], 'text': ''},
                      response_content={'compression': 0,
                                        'mimeType': 'text/html',
                                        'size': 277,
                                        'text': '<!DOCTYPE html>\n'
                                                '<html>\n'
                                                '<head>\n'
                                                '<title>Example</title>\n'
                                                '</head>\n'
                                                '<body>\n'
                                                '<form action="/myform" method="POST">\n'
                                                '<input type="text" name="name" value="You name"/>\n'
                                                '<input type="submit" value="Tell me your name"/>\n'
                                                '</form>\n'
                                                '</body>\n'
                                                '</html>\n'}):
    content = {
        '_zapMessageId': '1',
        '_zapMessageNote': '',
        '_zapMessageType': '1',
        'cache': {},
        'request': {'bodySize': 0,
                    'cookies': [],
                    'headers': request_headers,
                    'headersSize': 116,
                    'httpVersion': 'HTTP/1.1',
                    'method': 'GET',
                    'postData': request_post_data,
                    'queryString': [],
                    'url': url},
        'response': {'bodySize': 277,
                     'content': response_content,
                     'cookies': [],
                     'headers': response_headers,
                     'headersSize': 238,
                     'httpVersion': 'HTTP/1.1',
                     'redirectURL': '',
                     'status': status_code,
                     'statusText': 'OK'},
        'startedDateTime': '2020-05-05T06:39:43.835+00:00',
        'time': 30,
        'timings': {'receive': 30, 'send': 0, 'wait': 0},
    }

    return json.dumps(content)

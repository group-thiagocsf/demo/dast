from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, call, patch

from src.models.errors import InvalidAPISpecificationError, NoUrlsError
from src.services import APISpecification


class TestAPISpecification(TestCase):

    def test_loads_from_url_if_spec_is_url(self):
        zaproxy = MagicMock()
        zaproxy.import_api_spec_from_url.return_value = 'WARNINGS!'
        zaproxy.urls.return_value = ['http://url.com']
        api_spec = APISpecification(zaproxy, 'https://theintercept.com', 'https://override.test')
        urls = None

        with patch('src.services.api_specification.logging') as mock_logging:
            urls = api_spec.load()

        mock_logging.debug.assert_has_calls([
            call('Import OpenAPI URL https://theintercept.com'),
            call('Import warnings: WARNINGS!'),
        ])
        mock_logging.info.assert_called_once_with('Number of imported URLs: 1')
        zaproxy.import_api_spec_from_url.assert_called_once_with(
            'https://theintercept.com',
            'https://override.test',
        )
        zaproxy.urls.assert_called_once()

        self.assertEqual(urls, ['http://url.com'])

    def test_loads_from_file_if_spec_is_file(self):
        zaproxy = MagicMock()
        zaproxy.import_api_spec_from_file.return_value = 'WARNINGS!'
        zaproxy.urls.return_value = ['http://url.com']
        api_spec = APISpecification(zaproxy, 'api_spec.yaml', '')
        urls = None

        with patch.multiple(
            'src.services.api_specification',
            logging=DEFAULT,
        ) as mock_dependencies:
            with patch('os.path.exists', return_value=True):
                urls = api_spec.load()

        mock_logging = mock_dependencies['logging']

        mock_logging.debug.assert_has_calls([
            call('Import OpenAPI File api_spec.yaml'),
            call('Import warnings: WARNINGS!'),
        ])
        mock_logging.info.assert_called_once_with('Number of imported URLs: 1')
        zaproxy.import_api_spec_from_file.assert_called_once_with('/zap/wrk/api_spec.yaml')
        zaproxy.urls.assert_called_once()

        self.assertEqual(urls, ['http://url.com'])

    def test_raises_invalid_spec_if_spec_is_not_valid_url_or_file(self):
        api_spec = APISpecification(MagicMock(), 'invalid!', '')

        with patch('src.services.api_specification.os.path.exists', return_value=False):
            with self.assertRaises(InvalidAPISpecificationError) as error:
                api_spec.load()

        self.assertIn('Target must be either a valid URL or a local file', str(error.exception))
        self.assertIn('file does not exist: /zap/wrk/invalid!', str(error.exception))

    def test_load_raises_no_urls_if_no_urls_in_spec(self):
        zaproxy = MagicMock()
        zaproxy.urls.return_value = []
        api_spec = APISpecification(zaproxy, 'api_spec.yaml', '')

        with self.assertRaises(NoUrlsError) as error, \
             patch.multiple(
                 'src.services.api_specification',
                 os=DEFAULT,
             ) as mock_dependencies:
            mock_os = mock_dependencies['os']
            mock_os.path.exists.return_value = True

            api_spec.load()

        self.assertIn('Failed to import any URLs defined in API specification', str(error.exception))

    def test_load_logs_a_warning_when_using_host_override_with_file_spec(self):
        zaproxy = MagicMock()
        zaproxy.urls.return_value = ['https://vulnerable.api']
        api_spec = APISpecification(zaproxy, 'api_spec.yaml', 'https://host.override')

        with patch.multiple(
                 'src.services.api_specification',
                 logging=DEFAULT,
                 os=DEFAULT,
             ) as mock_dependencies:
            api_spec.load()

        mock_logging = mock_dependencies['logging']

        mock_logging.warning.assert_called_with(
            'Host override is not compatible with loading API specification '
            'from a file and will be ignored',
        )

    def test_raises_invalid_spec_if_spec_file_is_not_in_zap_wrk(self):
        api_spec = APISpecification(MagicMock(), '../../sensitive_file', '')

        with patch('src.services.api_specification.os.path.exists', return_value=True):
            with self.assertRaises(InvalidAPISpecificationError) as error:
                api_spec.load()

        self.assertIn('Target must be either a valid URL or a local file', str(error.exception))
        self.assertIn('file does not exist: /sensitive_file', str(error.exception))

from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase

from src.services.script_finder import ScriptFinder


class TestScriptFinder(TestCase):

    def test_should_find_http_sender_script(self):
        with TemporaryDirectory() as dirname:
            Path(f'{dirname}/httpsender').mkdir()
            Path(f'{dirname}/httpsender/script.js').touch()
            scripts = ScriptFinder([dirname]).find_scripts()

            self.assertEqual(1, len(scripts))
            self.assertEqual('script.js', scripts[0].name)
            self.assertEqual(f'{dirname}/httpsender/script.js', scripts[0].file_path)

    def test_should_find_many_http_sender_scripts(self):
        with TemporaryDirectory() as dirname:
            Path(f'{dirname}/httpsender').mkdir()
            Path(f'{dirname}/httpsender/script-a.js').touch()
            Path(f'{dirname}/httpsender/script-b.js').touch()
            scripts = ScriptFinder([dirname]).find_scripts()

            self.assertEqual(2, len(scripts))

    def test_should_find_scripts_from_all_directories(self):
        with TemporaryDirectory() as dirname_a, TemporaryDirectory() as dirname_b:
            Path(f'{dirname_a}/httpsender').mkdir()
            Path(f'{dirname_a}/httpsender/script-a.js').touch()
            Path(f'{dirname_b}/httpsender').mkdir()
            Path(f'{dirname_b}/httpsender/script-b.js').touch()

            scripts = ScriptFinder([dirname_a, dirname_b]).find_scripts()

            self.assertEqual(2, len(scripts))

    def test_should_not_find_scripts_when_there_are_none(self):
        with TemporaryDirectory() as dirname:
            Path(f'{dirname}/httpsender').mkdir()
            scripts = ScriptFinder([dirname]).find_scripts()

            self.assertEqual(0, len(scripts))

    def test_should_not_find_scripts_when_directory_is_not_present(self):
        scripts = ScriptFinder(['not.a.directory']).find_scripts()
        self.assertEqual(0, len(scripts))

    def test_should_not_find_scripts_when_script_type_directory_is_not_present(self):
        with TemporaryDirectory() as dirname:
            scripts = ScriptFinder([dirname]).find_scripts()
            self.assertEqual(0, len(scripts))

    def test_should_find_scripts_when_there_is_an_extra_slash(self):
        with TemporaryDirectory() as dirname:
            Path(f'{dirname}/httpsender').mkdir()
            Path(f'{dirname}/httpsender/script.js').touch()
            scripts = ScriptFinder([f'{dirname}/']).find_scripts()

            self.assertEqual(1, len(scripts))

    def test_should_ignore_scripts_that_have_a_js_extension(self):
        with TemporaryDirectory() as dirname:
            Path(f'{dirname}/httpsender').mkdir()
            Path(f'{dirname}/httpsender/script.py').touch()
            scripts = ScriptFinder([f'{dirname}/']).find_scripts()

            self.assertEqual(0, len(scripts))

    def test_should_ignore_directories(self):
        with TemporaryDirectory() as dirname:
            Path(f'{dirname}/httpsender').mkdir()
            Path(f'{dirname}/httpsender/script.js').mkdir()
            scripts = ScriptFinder([f'{dirname}/']).find_scripts()

            self.assertEqual(0, len(scripts))

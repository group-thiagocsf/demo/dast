from unittest import TestCase
from unittest.mock import mock_open, patch

from src.services.browserker import BrowserkerConfigurationFile
from src.zap_gateway import Settings
from test.unit.mock_config import ToConfig


class TestBrowserkerConfigurationFile(TestCase):

    def test_writes_configuration_file(self):
        config = ToConfig(target='http://my.site.com:3000/context/page.html',
                          zap_port=9000,
                          exclude_urls=[],
                          paths_to_scan_list=['path1', 'path2'],
                          paths_to_scan_file_path='paths.txt',
                          browserker_allowed_hosts=['my.site.com'],
                          browserker_excluded_hosts=[],
                          browserker_ignored_hosts=[],
                          browserker_max_actions=20000,
                          browserker_max_attack_failures=2,
                          browserker_max_depth=4,
                          browserker_number_of_browsers=7)
        output = self._build_file(config)

        self.assertIn('AllowedHosts = ["my.site.com"]', output)
        self.assertIn('DataPath = "/output/browserker_data"', output)
        self.assertIn('DirectPaths = ["path1","path2"]', output)
        self.assertIn('DirectPathsFilePath = "paths.txt"', output)
        self.assertIn('ShowBrowser = false', output)
        self.assertIn('ExcludedHosts = []', output)
        self.assertIn('ExcludedURIs = []', output)
        self.assertIn('IgnoredHosts = []', output)
        self.assertIn('ExcludedElements = []', output)
        self.assertIn('JSPluginPath = "/browserker/plugins/"', output)
        self.assertIn('LogLevel = "info"', output)
        self.assertIn('MaxActions = 20000', output)
        self.assertIn('MaxAttackFailures = 2', output)
        self.assertIn('MaxDepth = 4', output)
        self.assertIn('NumBrowsers = 7', output)
        self.assertIn('ScanMode = "crawl"', output)
        self.assertIn('PluginResourcePath = "/browserker/resources/"', output)
        self.assertIn('Proxy = "http://127.0.0.1:9000"', output)
        self.assertIn('URL = "http://my.site.com:3000/context/page.html"', output)
        self.assertIn('[FileLogLevels]', output)
        self.assertIn('LogLevel = "debug"', output)
        self.assertIn('[ConsoleLogLevels]', output)
        self.assertIn('LogLevel = "info"', output)

    def test_configures_excluded_urls(self):
        config = ToConfig(exclude_urls=['http://my.site/a', 'http://my.site/b/.*'])
        output = self._build_file(config)

        self.assertIn('ExcludedURIs = ["http://my.site/a", "http://my.site/b/.*"]', output)
        self.assertNotIn('ExcludedURIs = []', output)

    def test_configures_excluded_hosts(self):
        config = ToConfig(browserker_excluded_hosts=['site-a.com', '', 'site-b.com'])
        output = self._build_file(config)

        self.assertIn('ExcludedHosts = ["site-a.com", "site-b.com"]', output)

    def test_configures_allowed_hosts(self):
        config = ToConfig(browserker_allowed_hosts=['my.site.com', 'another-site.com'])
        output = self._build_file(config)

        self.assertIn('AllowedHosts = ["my.site.com", "another-site.com"]', output)

    def test_configures_ignored_hosts(self):
        config = ToConfig(browserker_ignored_hosts=['site-a.com', 'site-b.com'])
        output = self._build_file(config)

        self.assertIn('IgnoredHosts = ["site-a.com", "site-b.com"]', output)

    def test_configures_excluded_elements(self):
        config = ToConfig(browserker_excluded_elements=['css:.navigation-item'])
        output = self._build_file(config)

        self.assertIn('ExcludedElements = ["css:.navigation-item"]', output)

    def test_doesnt_exclude_authentication_page(self):
        config = ToConfig(exclude_urls=['http://my.site/login-page', 'http://my.site/large-download'],
                          auth_url='http://my.site/login-page')
        output = self._build_file(config)

        self.assertIn('ExcludedURIs = ["http://my.site/large-download"]', output)

    def test_configures_form_authentication(self):
        config = ToConfig(auth_url='http://my.site/login-page',
                          auth_username='usr', auth_password='pwd', exclude_urls=[])
        output = self._build_file(config)

        self.assertIn('[AuthDetails]', output)
        self.assertIn('UserName = "usr"', output)
        self.assertIn('Password = "pwd"', output)

    def test_does_not_configure_auth_if_no_auth_url(self):
        config = ToConfig(auth_username='usr', auth_password='pwd', exclude_urls=[])
        output = self._build_file(config)

        self.assertNotIn('[AuthDetails]', output)
        self.assertNotIn('UserName = "usr"', output)
        self.assertNotIn('Password = "pwd"', output)

    def test_configures_form_authentication_fields(self):
        config = ToConfig(auth_username='usr', auth_password='pwd',
                          auth_url='http://my.site/login-page',
                          auth_username_field='[id=username]',
                          auth_first_submit_field='[id=continue]',
                          auth_password_field='[type=password]',
                          auth_submit_field='[id=submit]',
                          auth_verification_url='http://site.com',
                          auth_verification_selector='css:.home',
                          auth_verification_login_form=True,
                          auth_report=True,
                          exclude_urls=[])
        output = self._build_file(config)

        self.assertIn('[AuthDetails]', output)
        self.assertIn('UserName = "usr"', output)
        self.assertIn('Password = "pwd"', output)
        self.assertIn('UserNameField = "[id=username]"', output)
        self.assertIn('UserNameSubmitField = "[id=continue]"', output)
        self.assertIn('PasswordField = "[type=password]"', output)
        self.assertIn('VerificationURL = "http://site.com"', output)
        self.assertIn('VerificationSelector = "css:.home"', output)
        self.assertIn('VerificationLoginForm = true', output)
        self.assertIn(f'ReportPath = "{Settings.WRK_DIR}gl-dast-debug-auth-report.html"', output)

    def test_configures_custom_request_headers(self):
        config = ToConfig(request_headers={'Accept': 'application/json', 'Cache-Control': 'no-cache'})
        output = self._build_file(config)

        self.assertIn('[CustomHeaders]', output)
        self.assertIn('Accept = "application/json"', output)
        self.assertIn('Cache-Control = "no-cache"', output)

    def test_configures_custom_cookies(self):
        config = ToConfig(browserker_cookies={'name': 'fred', 'sessionid': '123456789'})
        output = self._build_file(config)

        self.assertIn('[CustomCookies]', output)
        self.assertIn('name = "fred"', output)
        self.assertIn('sessionid = "123456789"', output)

    def test_configures_console_log_levels(self):
        config = ToConfig(browserker_log={'brows': 'info', 'auth': 'debug'})
        output = self._build_file(config)

        self.assertIn('[ConsoleLogLevels]', output)
        self.assertIn('BROWS = "info"', output)
        self.assertIn('AUTH = "debug"', output)

    def _build_file(self, config) -> str:
        with patch('src.services.browserker.browserker_configuration_file.open', mock_open()) as handle_builder:
            BrowserkerConfigurationFile(config, '/tmp/config.toml').write()

        mock_handle = handle_builder()
        return ''.join([call[1][0] for call in mock_handle.write.mock_calls])

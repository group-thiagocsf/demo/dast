from unittest import TestCase
from unittest.mock import call, patch

from src.services.active_scan_logger import ActiveScanLogger


class TestActiveScanLogger(TestCase):

    def test_log_logs_all_rules(self):
        scan_data = [
            'http://test.site',
            {
                'HostProcess': [
                    {
                        'Plugin': [
                            'Bad cookie',
                            '111',
                            'release',
                            'Complete',
                            '1',
                            '2',
                            '3',
                        ],
                    },
                    {
                        'Plugin': [
                            'Bad form',
                            '222',
                            'release',
                            'Complete',
                            '4',
                            '5',
                            '6',
                        ],
                    },
                ],
            },
            'http://another.site',
            {
                'HostProcess': [
                    {
                        'Plugin': [
                            'Bad SQL',
                            '333',
                            'release',
                            'Complete',
                            '7',
                            '8',
                            '9',
                        ],
                    },
                ],
            },
        ]
        logger = ActiveScanLogger(scan_data)

        with patch('src.services.active_scan_logger.logging') as mock_logging:
            logger.log()

        mock_logging.info.assert_has_calls([
            call(
                'Active scan rule Bad cookie [111] [Complete] ran on http://test.site, '
                'took 1ms, made 2 request(s), and found 3 alert(s).',
            ),
            call(
                'Active scan rule Bad form [222] [Complete] ran on http://test.site, '
                'took 4ms, made 5 request(s), and found 6 alert(s).',
            ),
            call(
                'Active scan rule Bad SQL [333] [Complete] ran on http://another.site, '
                'took 7ms, made 8 request(s), and found 9 alert(s).',
            ),
        ])

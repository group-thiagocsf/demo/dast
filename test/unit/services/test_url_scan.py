from unittest import TestCase
from unittest.mock import MagicMock, patch

from src.models import Target
from src.services import URLScan


class TestURLScan(TestCase):

    def setUp(self):
        self.target = Target('https://example.com')
        self.zap = MagicMock()
        self.urls_to_scan = [
            'https://example.com/1.html',
            'https://example.com/2.html',
            'https://example.com/3.xml',
        ]

    def test_with_urls_list_runs_url_scan_with_generated_file_path(self):
        url_scan = URLScan(self.zap, self.target, self.urls_to_scan, [])

        with patch('src.services.url_scan.UrlListFileWriter'):
            url_scan.run()

        self.zap.run_url_scan.assert_called_once_with(URLScan.URLS_TO_SCAN_PATH)

    def test_with_urls_list_runs_url_scan_with_excluded_url_pattern(self):
        url_scan = URLScan(self.zap, self.target, self.urls_to_scan, ['.+.xml'])

        with patch('src.services.url_scan.UrlListFileWriter') as file_writer:
            url_scan.run()

        file_writer.assert_called_once_with(['https://example.com/1.html', 'https://example.com/2.html'],
                                            URLScan.URLS_TO_SCAN_PATH)

    def test_with_urls_list_runs_url_and_with_excluds_urls(self):
        url_scan = URLScan(
            self.zap,
            self.target,
            self.urls_to_scan,
            ['https://example.com/1.html', 'https://example.com/2.html'])

        with patch('src.services.url_scan.UrlListFileWriter') as file_writer:
            url_scan.run()

        file_writer.assert_called_once_with(['https://example.com/3.xml'],
                                            URLScan.URLS_TO_SCAN_PATH)

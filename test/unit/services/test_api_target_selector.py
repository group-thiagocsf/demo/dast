from unittest import TestCase
from unittest.mock import call, patch

from src.services import APITargetSelector


class TestAPITargetSelector(TestCase):

    def test_select_uses_first_given_url_as_target(self):
        selector = APITargetSelector(['https://api.target'])
        target = None

        with patch('src.services.api_target_selector.logging') as mock_logging:
            target = selector.select()

        mock_logging.debug.assert_called_once_with(
            'Setting target to URL from API specification: https://api.target',
        )

        self.assertEqual(target, 'https://api.target')

    def test_select_uses_host_override_if_present(self):
        selector = APITargetSelector(['https://api.target'], 'https://host.override')
        target = None

        with patch('src.services.api_target_selector.logging') as mock_logging:
            target = selector.select()

        mock_logging.debug.assert_has_calls([
            call('Setting target to URL from API specification: https://api.target'),
            call('Setting target to new URL with host override: https://host.override'),
        ])

        self.assertEqual(target, 'https://host.override')

    def test_select_adds_scheme_to_host_override(self):
        selector = APITargetSelector(['https://api.target'], 'host.override')
        target = None

        with patch('src.services.api_target_selector.logging'):
            target = selector.select()

        self.assertEqual(target, 'https://host.override')

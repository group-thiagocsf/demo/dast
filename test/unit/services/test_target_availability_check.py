import unittest
from unittest.mock import MagicMock, Mock

from src.services.target_availability_check import TargetAvailabilityCheck
from test.unit.mock_config import ToConfig


class TestTargetAvailabilityCheck(unittest.TestCase):

    def test_is_safe_to_scan_when_not_full_scanning_and_site(self):
        response = MagicMock(headers={'gitlab-dast-permission': 'allow'})
        config = ToConfig(dast_major_version=1, full_scan=False, full_scan_domain_validation_required=True)
        check = TargetAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertEqual(error_message, '')

    def test_is_unsafe_to_scan_when_full_scanning_and_site_is_not_available(self):
        config = ToConfig(dast_major_version=1, full_scan=True, full_scan_domain_validation_required=False)
        check = TargetAvailabilityCheck(False, config, response=None)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, False)
        self.assertIn('site is unavailable', error_message)

    def test_is_safe_to_scan_when_no_domain_validation_required_and_site_does_not_deny_scan(self):
        response = MagicMock(headers={'gitlab-dast-permission': 'allow'})
        config = ToConfig(dast_major_version=1, full_scan=True, full_scan_domain_validation_required=False)
        check = TargetAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertEqual(error_message, '')

    def test_is_unsafe_to_scan_when_no_domain_validation_required_and_site_denys_scan(self):
        response = MagicMock(headers={'gitlab-dast-permission': 'deny'})
        config = ToConfig(dast_major_version=1, full_scan=True, full_scan_domain_validation_required=False)
        check = TargetAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, False)
        self.assertIn('application has explicitly denied DAST scans', error_message)

    def test_is_safe_to_scan_always_true_after_dast_2(self):
        response = Mock()
        config = ToConfig(
            dast_major_version=2, full_scan=True, full_scan_domain_validation_required=True, is_api_scan=False,
        )
        check = TargetAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()

        self.assertEqual(is_safe, True)
        self.assertEqual(error_message, '')

    def test_is_safe_to_scan_when_domain_validation_required_and_site_does_allows_scan(self):
        response = MagicMock(headers={'gitlab-dast-permission': 'allow'})
        config = ToConfig(dast_major_version=1, full_scan=True, full_scan_domain_validation_required=True)
        check = TargetAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertEqual(error_message, '')

    def test_is_safe_to_scan_when_domain_validation_required_and_site_does_does_not_allow_scan(self):
        response = MagicMock(headers={'gitlab-dast-permission': ''})
        config = ToConfig(dast_major_version=1, full_scan=True, full_scan_domain_validation_required=True)
        check = TargetAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, False)
        self.assertIn('application has not explicitly allowed DAST', error_message)

    def test_is_safe_to_scan_apis(self):
        response = MagicMock(headers={'gitlab-dast-permission': 'deny'})
        config = ToConfig(
            dast_major_version=1, full_scan=True, is_api_scan=True, full_scan_domain_validation_required=True,
        )
        check = TargetAvailabilityCheck(True, config, response=response)

        is_safe, error_message = check.is_safe_to_scan()
        self.assertEqual(is_safe, True)
        self.assertIn(error_message, '')

    def test_status_code_returns_status_code_from_response(self):
        response = Mock(status_code=502)
        check = TargetAvailabilityCheck(True, ToConfig(), response=response)

        self.assertEqual(check.status_code(), 502)

    def test_status_code_returns_None_when_there_is_no_response(self):
        check = TargetAvailabilityCheck(True, ToConfig())

        self.assertIsNone(check.status_code())

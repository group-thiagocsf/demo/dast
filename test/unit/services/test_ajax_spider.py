from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, Mock, call, patch

from src.services import AJAXSpider
from test.unit.factories.models import f_target


class AJAXSpiderTest(TestCase):

    def setUp(self):
        self.spider_status = 'starting'
        self.mock_spider_poll_delay = Mock()
        self.default_spider_poll = AJAXSpider.SPIDER_POLL_DELAY_SECONDS
        AJAXSpider.SPIDER_POLL_DELAY_SECONDS = self.mock_spider_poll_delay

        self.zap = MagicMock()
        self.zap.ajax_spider_status.side_effect = self._mock_spider_status

    def tearDown(self):
        AJAXSpider.SPIDER_POLL_DELAY_SECONDS = self.default_spider_poll

    def test_run_runs_ajax_spider(self):
        target = f_target('https://testwebsite.com')
        spider = AJAXSpider(self.zap, target, 0)

        with patch.multiple(
            'src.services.ajax_spider',
            logging=DEFAULT,
            time=DEFAULT,
        ) as mock_dependencies:
            spider.run()

        mock_time = mock_dependencies['time']

        mock_time.sleep.assert_has_calls([
            call(self.mock_spider_poll_delay),
            call(self.mock_spider_poll_delay),
        ])
        self.zap.run_ajax_spider.assert_called_once_with(target)
        self.zap.ajax_spider_status.assert_has_calls([call(), call()])

    def test_run_sets_max_mins_to_run_if_configured(self):
        target = f_target('https://testwebsite.com')
        spider = AJAXSpider(self.zap, target, 666)

        with patch.multiple(
            'src.services.ajax_spider',
            logging=DEFAULT,
            time=DEFAULT,
        ):
            spider.run()

        self.zap.set_ajax_spider_maximum_run_time.assert_called_once_with('666')

    def test_run_logs_spider_progress(self):
        self.zap.ajax_spider_results_count.return_value = 420
        target = f_target('https://testwebsite.com')
        spider = AJAXSpider(self.zap, target, 0)

        with patch.multiple(
            'src.services.ajax_spider',
            logging=DEFAULT,
            time=DEFAULT,
        ) as mock_dependencies:
            spider.run()

        mock_logging = mock_dependencies['logging']

        mock_logging.info.assert_has_calls([
            call('Ajax Spider starting with target: https://testwebsite.com'),
            call('Ajax Spider running: found 420 URLs so far'),
            call('Ajax Spider complete: found 420 URLs'),
        ])

    def _mock_spider_status(self):
        if self.spider_status == 'running':
            self.spider_status = 'finished'
        else:
            self.spider_status = 'running'

        return self.spider_status

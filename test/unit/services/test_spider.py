from unittest import TestCase
from unittest.mock import DEFAULT, MagicMock, Mock, call, patch

from src.services import Spider
from test.unit.factories.models import f_target


class TestSpider(TestCase):

    def setUp(self):
        self.mock_spider_poll_delay = Mock()
        self.default_spider_poll = Spider.SPIDER_POLL_DELAY_SECONDS
        Spider.SPIDER_POLL_DELAY_SECONDS = self.mock_spider_poll_delay

        self.spider_id = Mock()
        self.spider_progress_percentage = 0
        self.zap = MagicMock()
        self.zap.spider_progress_percentage.side_effect = self._mock_spider_progress_percentage
        self.zap.run_spider.return_value = self.spider_id

    def tearDown(self):
        Spider.SPIDER_POLL_DELAY_SECONDS = self.default_spider_poll

    def test_run_runs_spider(self):
        target = f_target('http://test.target')
        spider = Spider(self.zap, target)

        with patch.multiple('src.services.spider', logging=DEFAULT, time=DEFAULT) as mock_dependencies:
            spider.run()

        mock_time = mock_dependencies['time']

        self.zap.run_spider.assert_called_once_with(target)
        self.zap.spider_progress_percentage.assert_has_calls([call(self.spider_id), call(self.spider_id)])
        mock_time.sleep.assert_has_calls([
            call(self.mock_spider_poll_delay),
            call(self.mock_spider_poll_delay),
        ])

    def test_run_logs_spider_progress(self):
        target = f_target('http://test.target')
        spider = Spider(self.zap, target)

        with patch.multiple('src.services.spider', logging=DEFAULT, time=DEFAULT) as mock_dependencies:
            spider.run()

        mock_logging = mock_dependencies['logging']

        mock_logging.info.assert_has_calls([
            call('Spider starting with target: http://test.target'),
            call('Spider progress: 50% complete'),
            call('Spider complete'),
        ])

    def _mock_spider_progress_percentage(self, _):
        if self.spider_progress_percentage == 50:
            self.spider_progress_percentage = 100
        else:
            self.spider_progress_percentage = 50

        return self.spider_progress_percentage

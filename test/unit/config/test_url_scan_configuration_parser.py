from unittest import TestCase
from unittest.mock import mock_open, patch

from src.config import InvalidConfigurationError
from src.config.url_scan_configuration_parser import URLScanConfigurationParser


def _mock_wrk_dir(path):
    return path.startswith('/zap/wrk')


def _mock_ci_project_dir(path):
    return path.startswith('/builds')


class TestURLScanConfigurationParser(TestCase):

    def setUp(self) -> None:
        self.target = 'https://example.com'
        self.target_env_var = 'DAST_WEBSITE'

    def test_when_paths_to_scan_list_returns_urls_to_scan(self):
        paths_to_scan_list = ['/', '/1', '/2?arg=1']
        urls_to_scan = URLScanConfigurationParser(self.target, paths_to_scan_list, '', self.target_env_var).parse()

        self.assertEqual(urls_to_scan, ['https://example.com/', 'https://example.com/1', 'https://example.com/2?arg=1'])

    @patch('os.path.exists', side_effect=_mock_wrk_dir)
    def test_when_paths_to_scan_file_path_returns_urls_to_scan_in_wrk_dir(self, _):
        urls_to_scan = URLScanConfigurationParser(self.target, [], '/path/to/file.txt', self.target_env_var)

        with patch('builtins.open', mock_open(read_data='test.html\ntest1.html')):
            urls_to_scan = urls_to_scan.parse()

        self.assertEqual(urls_to_scan, ['https://example.com/test.html', 'https://example.com/test1.html'])

    @patch('os.path.exists', side_effect=_mock_ci_project_dir)
    @patch.dict('os.environ', {'CI_PROJECT_DIR': '/builds'})
    def test_when_paths_to_scan_file_path_returns_urls_to_scan_in_ci_project_dir(self, _):
        urls_to_scan = URLScanConfigurationParser(self.target, [], '/path/to/file.txt', self.target_env_var)

        with patch('builtins.open', mock_open(read_data='test.html\ntest1.html')):
            urls_to_scan = urls_to_scan.parse()

        self.assertEqual(urls_to_scan, ['https://example.com/test.html', 'https://example.com/test1.html'])

    @patch.dict('os.environ', {}, clear=True)
    def test_when_ci_project_dir_does_not_exist(self):
        urls_to_scan = URLScanConfigurationParser(self.target, [], '/path/to/file.txt', self.target_env_var)

        with self.assertRaises(InvalidConfigurationError) as error:
            urls_to_scan = urls_to_scan.parse()

        self.assertIn(
            'CI_PROJECT_DIR must be available when using DAST_PATHS_FILE outside of /zap/wrk',
            str(error.exception),
        )

    @patch('os.path.exists', return_value=True)
    def test_wrk_dir_takes_precedence_over_builds_dir(self, _):
        urls_to_scan = URLScanConfigurationParser(self.target, [], 'path/to/file.txt', self.target_env_var)

        with patch('builtins.open', mock_open(read_data='test.html\ntest1.html')) as m_open:
            urls_to_scan = urls_to_scan.parse()

        m_open.assert_called_once_with('/zap/wrk/path/to/file.txt', 'r')
        self.assertEqual(urls_to_scan, ['https://example.com/test.html', 'https://example.com/test1.html'])

    @patch('os.path.exists', return_value=True)
    @patch.dict('os.environ', {'CI_PROJECT_DIR': '/builds'})
    def test_when_paths_to_scan_file_path_is_an_unauthorized_file(self, _):
        urls_to_scan = URLScanConfigurationParser(self.target, [], '/../../restricted.txt', self.target_env_var)

        with self.assertRaises(InvalidConfigurationError) as error:
            urls_to_scan = urls_to_scan.parse()

        self.assertIn('/../../restricted.txt could not be found in either /builds/ or /zap/wrk/', str(error.exception))

    @patch.dict('os.environ', {'CI_PROJECT_DIR': '/builds'})
    def test_when_paths_to_scan_file_path_does_not_exist(self):
        urls_to_scan = URLScanConfigurationParser(self.target, [], '/does/not/exist.txt', self.target_env_var)

        with self.assertRaises(InvalidConfigurationError) as error:
            with patch('os.path.exists', return_value=False):
                urls_to_scan = urls_to_scan.parse()

        self.assertIn(
            '/does/not/exist.txt could not be found in either /builds/ or /zap/wrk/',
            str(error.exception),
        )

    def test_raises_error_when_paths_to_scan_list_is_specified_but_target_is_not(self):
        with self.assertRaises(Exception) as error:
            URLScanConfigurationParser('', ['/page.html', 'page2.html'], '', self.target_env_var).parse()

        self.assertIn('When DAST_PATHS is defined DAST_WEBSITE must also be set.', str(error.exception))

    def test_raises_error_when_paths_to_scan_file_path_is_specified_but_target_is_not(self):
        with self.assertRaises(Exception) as error:
            URLScanConfigurationParser('', [], '/path/to/paths_file.txt', self.target_env_var).parse()

        self.assertIn('When DAST_PATHS_FILE is defined DAST_WEBSITE must also be set.', str(error.exception))

    def test_raises_error_when_paths_to_scan_list_and_paths_to_scan_file_path_are_both_specified(self):
        with self.assertRaises(Exception) as error:
            URLScanConfigurationParser(
                'http://example.com', ['/page.html', 'page2.html'],
                '/path/to/paths_file.txt', self.target_env_var).parse()

        self.assertIn('DAST_PATHS and DAST_PATHS_FILE can not be defined at the same time.', str(error.exception))

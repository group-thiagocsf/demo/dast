from unittest import TestCase
from unittest.mock import MagicMock

from src.config.fallback_to_environment import FallbackToEnvironment


class TestFallbackToEnvironment(TestCase):

    def test_sets_metavar(self):
        fallback = FallbackToEnvironment(
            ['EXAMPLE_ENV_VAR', 'OLD_EXAMPLE_ENV_VAR'], MagicMock(), dest='test', option_strings=[])

        self.assertEqual(fallback.metavar, 'EXAMPLE_ENV_VAR')

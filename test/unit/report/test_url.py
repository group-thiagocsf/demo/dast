import unittest

from src.report import URL


class TestUrl(unittest.TestCase):

    def test_should_parse_http_url_all_parts_defined(self):
        url = URL.parse('http://user:password@mysite.com:80/path?query=search#fragment')

        self.assertEqual('http', url.scheme)
        self.assertEqual('mysite.com', url.host)
        self.assertEqual(80, url.port)
        self.assertEqual('/path?query=search#fragment', url.path_and_query_fragment)
        self.assertEqual(False, url.is_secure)

    def test_should_assume_port_80_when_http_and_not_defined(self):
        url = URL.parse('http://mysite.com')
        self.assertEqual(80, url.port)

    def test_should_parse_https_scheme(self):
        url = URL.parse('https://mysite.com')
        self.assertEqual('https', url.scheme)

    def test_should_assume_port_443_when_https_and_not_defined(self):
        url = URL.parse('https://mysite.com')
        self.assertEqual(443, url.port)

    def test_scheme_and_host(self):
        self.assertEqual('http://mysite.com', URL.parse('http://u:p@mysite.com:80/path?query=q#f').scheme_and_host)
        self.assertEqual('http://mysite.com', URL.parse('http://mysite.com').scheme_and_host)
        self.assertEqual('http://mysite.com', URL.parse('http://mysite.com/').scheme_and_host)
        self.assertEqual('http://mysite.com:81', URL.parse('http://mysite.com:81').scheme_and_host)
        self.assertEqual('https://mysite.com', URL.parse('https://mysite.com:443').scheme_and_host)
        self.assertEqual('https://mysite.com', URL.parse('https://mysite.com').scheme_and_host)
        self.assertEqual('https://mysite.com:444', URL.parse('https://mysite.com:444').scheme_and_host)
        self.assertEqual('http://yoursite.com', URL.parse('http://yoursite.com').scheme_and_host)
        self.assertEqual('http://yoursite.com:443', URL.parse('http://yoursite.com:443').scheme_and_host)
        self.assertEqual('https://yoursite.com:80', URL.parse('https://yoursite.com:80').scheme_and_host)

    def test_is_secure(self):
        self.assertEqual(True, URL.parse('https://mysite.com').is_secure)
        self.assertEqual(False, URL.parse('http://mysite.com').is_secure)

    def test_path_and_query_fragment(self):
        self.assertEqual('/path?query=q#f', URL.parse('http://me.com/path?query=q#f').path_and_query_fragment)
        self.assertEqual('/path?query=q', URL.parse('http://me.com/path?query=q').path_and_query_fragment)
        self.assertEqual('/path#f', URL.parse('http://me.com/path#f').path_and_query_fragment)
        self.assertEqual('', URL.parse('http://me.com').path_and_query_fragment)
        self.assertEqual('/', URL.parse('http://me.com/').path_and_query_fragment)
        self.assertEqual('?foo=bar', URL.parse('http://me.com?foo=bar').path_and_query_fragment)
        self.assertEqual('#page-1', URL.parse('http://me.com#page-1').path_and_query_fragment)

    def test_should_complain_when_not_a_valid_url(self):
        try:
            URL.parse('')
            self.assertTrue(False)
        except RuntimeError as e:
            self.assertIn("Failed to parse '' as it is not a valid URL", str(e))

    def test_urls_can_be_sorted_by_schema_host(self):
        a = URL.parse('http://a.site')
        aa = URL.parse('https://a.site')
        b = URL.parse('http://b.site')
        c = URL.parse('http://c.site')

        in_order = sorted([a, c, aa, b])
        self.assertEqual('http://a.site', in_order[0].scheme_and_host)
        self.assertEqual('http://b.site', in_order[1].scheme_and_host)
        self.assertEqual('http://c.site', in_order[2].scheme_and_host)
        self.assertEqual('https://a.site', in_order[3].scheme_and_host)

    def test_can_be_converted_to_a_string(self):
        self.assertEqual('http://mysite.com/path?query=q#f', str(URL.parse('http://u:p@mysite.com:80/path?query=q#f')))

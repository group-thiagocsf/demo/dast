import unittest
from datetime import datetime, timezone

from src.models.http import HttpMessages
from src.report.security_report_formatters import ScanReportFormatter
from test.unit.factories.models import http as http_factories
from test.unit.mock_config import ToConfig


class TestScanReportFormatter(unittest.TestCase):

    def setUp(self):
        self.messages = [
            http_factories.http_message(request=http_factories.http_request(method='GET',
                                                                            url='http://nginx/')),
            http_factories.http_message(request=http_factories.http_request(method='POST',
                                                                            url='http://nginx/a.txt')),
            http_factories.http_message(request=http_factories.http_request(method='GET',
                                                                            url='http://nginx/a.txt')),
            http_factories.http_message(request=http_factories.http_request(method='GET',
                                                                            url='http://nginx/robots.txt')),
            http_factories.http_message(request=http_factories.http_request(method='POST',
                                                                            url='http://nginx/myform')),
        ]

        self.config = ToConfig(browserker_scan=False)
        self.scanned_resources = HttpMessages(self.messages)
        self.zap_version = 'D-2019-09-23'
        utc = timezone.utc
        self.start_date_time = datetime(2020, 5, 21, 19, 26, 43, 50218, utc)
        self.end_date_time = datetime(2020, 5, 21, 20, 46, 43, 50218, utc)
        self.report = ScanReportFormatter(self.start_date_time, self.config).format_report(self.scanned_resources,
                                                                                           self.zap_version,
                                                                                           self.end_date_time)

    def test_end_time(self):
        self.assertEqual(self.report['end_time'], '2020-05-21T20:46:43')

    def test_start_time(self):
        self.assertEqual(self.report['start_time'], '2020-05-21T19:26:43')

    def test_scanner_version(self):
        self.assertEqual(self.report['scanner']['version'], self.zap_version)

    def test_scanner_url(self):
        self.assertEqual(self.report['scanner']['url'], 'https://www.zaproxy.org')

    def test_scanner_name(self):
        self.assertEqual(self.report['scanner']['name'], 'OWASP Zed Attack Proxy (ZAP)')

    def test_scanner_vendor(self):
        self.assertEqual(self.report['scanner']['vendor']['name'], 'GitLab')

    def test_scanner_changes_when_browserker_scan(self):
        self.config.browserker_scan = True
        report = ScanReportFormatter(self.start_date_time, self.config).format_report(self.scanned_resources,
                                                                                      self.zap_version,
                                                                                      self.end_date_time)

        self.assertEqual(report['scanner']['id'], 'zaproxy-browserker')
        self.assertEqual(report['scanner']['name'], 'OWASP Zed Attack Proxy (ZAP) and Browserker')

    def test_scanner_id(self):
        self.assertEqual(self.report['scanner']['id'], 'zaproxy')

    def test_type_is_dast(self):
        self.assertEqual(self.report['type'], 'dast')

    def test_status_is_success(self):
        self.assertEqual(self.report['status'], 'success')

    def test_messages_is_an_empty_array(self):
        self.assertEqual(self.report['messages'], [])

    def test_scanned_resources_include_all_properties(self):
        self.assertEqual(len(self.report['scanned_resources']), 5)

        scanned_resource = self.report['scanned_resources'][4]
        self.assertEqual(scanned_resource['type'], 'url')
        self.assertEqual(scanned_resource['url'], 'http://nginx/robots.txt')
        self.assertEqual(scanned_resource['method'], 'GET')

    def test_scanned_resources_should_be_sorted_by_type_url_and_method(self):
        resource_0 = self.report['scanned_resources'][0]
        self.assertEqual(resource_0['url'], 'http://nginx/')
        self.assertEqual(resource_0['method'], 'GET')

        resource_1 = self.report['scanned_resources'][1]
        self.assertEqual(resource_1['url'], 'http://nginx/a.txt')
        self.assertEqual(resource_1['method'], 'GET')

        resource_2 = self.report['scanned_resources'][2]
        self.assertEqual(resource_2['url'], 'http://nginx/a.txt')
        self.assertEqual(resource_2['method'], 'POST')

        resource_3 = self.report['scanned_resources'][3]
        self.assertEqual(resource_3['url'], 'http://nginx/myform')
        self.assertEqual(resource_3['method'], 'POST')

        resource_4 = self.report['scanned_resources'][4]
        self.assertEqual(resource_4['url'], 'http://nginx/robots.txt')
        self.assertEqual(resource_4['method'], 'GET')

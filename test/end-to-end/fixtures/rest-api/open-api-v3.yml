openapi: 3.0.0
info:
  title: Trees API
  description: API to return some lovely trees
  version: 1.0.0
paths:
  /:
    get:
      summary: Entrypoint
      description: You should depend on a version of the API directly. Latest version at
        time of writing is `v1`
      responses:
        "302":
          description: Redirect to the latest version of the API
  /v1:
    get:
      summary: Version entrypoint
      description: You should *not* call this directly
      responses:
        "302":
          description: Redirect to the known trees
  /v1/trees:
    get:
      summary: Trees
      description: Lists known trees
      responses:
        "200":
          description: List of known trees
          content:
            application/json:
              schema:
                type: object
                required:
                  - tree
                properties:
                  tree:
                    type: array
                    items:
                      $ref: "#/components/schemas/TreeRef"
    post:
      summary: Creates a new tree
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Tree"
        description: The name of the tree
      responses:
        "201":
          description: Tree is created
  "/v1/tree/{treeId}":
    get:
      summary: Returns a tree
      parameters:
        - in: path
          name: treeId
          required: true
          description: Unique ID of the tree
          schema:
            type: integer
            minimum: 1
      responses:
        "200":
          description: The details of the requested tree
        "404":
          description: Tree not found
    delete:
      summary: Deletes a tree
      parameters:
        - in: path
          name: treeId
          required: true
          description: Unique ID of the tree
          schema:
            type: integer
            minimum: 1
      responses:
        "204":
          description: The requested tree has been deleted
servers:
  - url: http://hostname:8080
components:
  schemas:
    Tree:
      type: object
      required:
        - tree
      properties:
        tree:
          type: object
          required:
            - name
          properties:
            name:
              type: string
    TreeRef:
      type: object
      required:
        - tree
      properties:
        tree:
          type: object
          required:
            - ref
          properties:
            ref:
              type: string

#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_multi_page_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_url_baseline_scan() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_PATHS=/page1.html,/page2.html \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_baseline_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_url_baseline_scan.json

  diff -u <(./normalize_dast_report.py expect/test_url_baseline_scan.json) \
          <(./normalize_dast_report.py output/report_test_url_baseline_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_url_full_scan() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_PATHS=page1.html,/page2.html,/page4.html \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_AUTH_EXCLUDE_URLS=http://nginx/page4.html \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_full_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_url_full_scan.json

  diff -u <(./normalize_dast_report.py expect/test_url_full_scan.json) \
          <(./normalize_dast_report.py output/report_test_url_full_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_url_scan_using_zap_wrk_file_path() {
  docker run --rm \
    -v "${PWD}/fixtures/url-scan":/zap/wrk \
    --network test \
    --env DAST_PATHS_FILE=paths_to_scan.txt \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_scan_using_zap_wrk_file_path.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < "${PWD}/fixtures/url-scan/gl-dast-report.json" > output/report_test_url_scan_using_zap_wrk_file_path.json

  diff -u <(./normalize_dast_report.py expect/test_url_scan_using_zap_wrk_file_path.json) \
          <(./normalize_dast_report.py output/report_test_url_scan_using_zap_wrk_file_path.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_url_scan_using_builds_file_path() {
  DAST_ROOT_DIR=${CI_PROJECT_DIR:-$PWD/../..}
  CI_PROJECT_DIR=${CI_PROJECT_DIR:-/builds}

  docker run --rm \
    -v "${DAST_ROOT_DIR}:${CI_PROJECT_DIR}" \
    -v "${PWD}":/output \
    --network test \
    --env CI_PROJECT_DIR="${CI_PROJECT_DIR}" \
    --env DAST_PATHS_FILE="test/end-to-end/fixtures/url-scan/paths_to_scan.txt" \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_scan_using_builds_file_path.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_url_scan_using_builds_file_path.json

  diff -u <(./normalize_dast_report.py expect/test_url_scan_using_builds_file_path.json) \
          <(./normalize_dast_report.py output/report_test_url_scan_using_builds_file_path.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

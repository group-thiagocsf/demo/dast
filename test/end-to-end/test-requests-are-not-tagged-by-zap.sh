#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

setup_suite() {
  setup_test_dependencies

  docker network create nextcloud-test >/dev/null

  docker run --rm \
    --name nextcloud \
    --network nextcloud-test \
    --env SQLITE_DATABASE=nextcloud \
    --env NEXTCLOUD_ADMIN_USER=admin \
    --env NEXTCLOUD_ADMIN_PASSWORD=XXX \
    --env NEXTCLOUD_TRUSTED_DOMAINS=nextcloud \
    -d \
    registry.gitlab.com/gitlab-org/security-products/dast/test-images/nextcloud:19.0.10-apache >/dev/null

  true
}

teardown_suite() {
  docker rm --force nextcloud  >/dev/null 2>&1
  docker network rm nextcloud-test >/dev/null 2>&1
  true
}

test_passive_scan_does_not_tag_requests() {
  docker run --rm \
    -v "${PWD}":/output \
    --network nextcloud-test \
    "${BUILT_IMAGE}" /analyze -d -j -t http://nextcloud/ >output/test_passive_scan_does_not_tag_requests.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q 'Passive Scan rule html_mailto took:' output/test_passive_scan_does_not_tag_requests.log
  assert_equals "1" "$?" "HTML tagging rule should not be configured"
}

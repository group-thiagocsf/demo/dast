#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null

  docker run \
    --name pancakes \
    --network test \
    -v "${PWD}/fixtures/pancakes":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/pancakes/nginx.conf":/etc/nginx/conf.d/default.conf \
    -d nginx:1.17.6-alpine >/dev/null

  true
}

teardown_suite() {
  docker rm --force pancakes  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_scan() {
  # deliberately testing legacy naming DAST_BROWSERKER_*
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_EXCLUDE_RULES=10096,10050,10027 \
    --env DAST_BROWSERKER_SCAN=true \
    --env DAST_BROWSER_EXCLUDED_HOSTS="fonts.googleapis.com" \
    --env DAST_BROWSER_IGNORED_HOSTS="unpkg.com" \
    --env DAST_BROWSER_COOKIES="dast_scan: browserker" \
    --env DAST_BROWSER_LOG="brows:debug" \
    --env DAST_REQUEST_HEADERS="x-scanner: browserker, x-scanner-app: pancake" \
    --env DAST_MASK_HTTP_HEADERS="" \
    --env DAST_BROWSER_EXCLUDED_ELEMENTS="a[href='/pancake/2']" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://pancakes >output/test_browserker_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . <gl-dast-report.json >output/report_test_browserker_scan.json
  rm -rf ./browserker_data
  mv ./browserker.log output/report_test_browserker_scan_browserker.log
  mv ./browserker-debug.log output/report_test_browserker_scan_debug.log
  mv ./report.dot output/report_test_browserker_scan_report.dot
  mv ./findings.json output/report_test_browserker_scan_findings.json

  diff -u <(./normalize_dast_report.py expect/test_browserker_scan.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  grep -q "Adding Browserker setting AllowedHosts = \[\"pancakes\"]" output/test_browserker_scan.log &&
  grep -q "Crawled path.*LoadURL \[http://pancakes]" output/report_test_browserker_scan_browserker.log &&
  grep -q "DBG BROWS" output/report_test_browserker_scan_browserker.log
  assert_equals "0" "$?" "Browserker log output different than expected"

  grep -q "dast_scan=browserker" output/report_test_browserker_scan.json
  assert_equals "0" "$?" "Report does not include added cookie"

  grep -q "x-scanner" output/report_test_browserker_scan.json
  assert_equals "0" "$?" "Report does not include x-scanner request header"

  grep -q "x-scanner-app" output/report_test_browserker_scan.json
  assert_equals "0" "$?" "Report does not include x-scanner-app request header"

  ./verify-dast-schema.py output/report_test_browserker_scan.json
}

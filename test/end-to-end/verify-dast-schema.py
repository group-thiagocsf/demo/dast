#!/usr/bin/env python

import json
import os
import sys
from typing import Any, Dict

import jsonschema
import requests


class SecureReport:

    def __init__(self, version: str, contents: Dict[str, Any]):
        self.version = version
        self.contents = contents

    def validate(self) -> None:
        schema = SecureSchemaGateway().download(self.version)
        schema.validate(self.contents)


class SecureReportParser:

    def load(self, report_file: str) -> SecureReport:
        if not os.path.exists(report_file):
            print(f'Cannot find {report_file}, aborting.')
            sys.exit(1)

        contents = json.loads(open(sys.argv[1]).read())

        if 'version' not in contents:
            raise RuntimeError('Failed to find version field in report, aborting.')

        return SecureReport(contents['version'], contents)


class SecureReportFormatSchema:

    def __init__(self, schema: Dict[str, Any]):
        self.schema = schema

    def validate(self, report: Dict[str, Any]) -> None:
        jsonschema.validate(instance=report, schema=self.schema)


class SecureSchemaGateway:

    def __init__(self):
        self.schema_directory = f'{os.path.dirname(os.path.abspath(__file__))}/downloaded'
        self.project_url = 'https://gitlab.com/gitlab-org/security-products/security-report-schemas'

    def download(self, schema_version):
        os.makedirs(self.schema_directory, exist_ok=True)

        local_schema = f'{self.schema_directory}/dast-schema-{schema_version}.json'
        schema_url = f'{self.project_url}/-/raw/v{schema_version}/dist/dast-report-format.json'

        if not os.path.exists(local_schema):
            response = requests.get(schema_url)
            response.raise_for_status()
            open(local_schema, 'wb').write(response.content)

        return SecureReportFormatSchema(json.loads(open(local_schema).read()))


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f'usage: {sys.argv[0]} file-to-verify')
        sys.exit(1)

    report = SecureReportParser().load(sys.argv[1])
    report.validate()

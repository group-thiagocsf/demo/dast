#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_ajax_spider_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_ajax_spider_starts_at_target_url() {
  docker run --rm -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" \
    /analyze -j -t http://nginx/food.html -d \
    >output/test_ajax_spider_starts_at_target_url.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_ajax_spider_starts_at_target_url.json

  jq -e '.scan.scanned_resources[] | select(.url | contains("bacon.html"))' 2>&1 >/dev/null <output/report_test_ajax_spider_starts_at_target_url.json
  assert_equals "0" "$?" "expected dynamic url bacon.html to be scanned, but it was not"

  jq -e '.scan.scanned_resources[] | select(.url | contains("vegetables.html"))' 2>&1 >/dev/null <output/report_test_ajax_spider_starts_at_target_url.json
  assert_equals "0" "$?" "expected dynamic url vegetables.html to be scanned, but it was not"

  grep '"uri": "http://nginx/bacon.html"' gl-dast-report.json >/dev/null
  assert_equals "0" "$?" "expected vulnerabilities to be found for dynamic url bacon.html, but there were none"

  grep '"uri": "http://nginx/vegetables.html"' gl-dast-report.json >/dev/null
  assert_equals "0" "$?" "expected vulnerabilities to be found for dynamic url vegetables.html, but there were none"

  ./verify-dast-schema.py output/report_test_ajax_spider_starts_at_target_url.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"

  diff -u <(./normalize_dast_report.py expect/test_ajax_spider_starts_at_target_url.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_dynamic_urls_can_be_excluded_from_scan() {
  docker run --rm -v "${PWD}":/output \
    --network test \
    --env DAST_USE_AJAX_SPIDER=true \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://nginx/food.html \
      --auth-exclude-urls 'http://nginx/bacon.html' \
     >output/test_dynamic_urls_can_be_excluded_from_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_dynamic_urls_can_be_excluded_from_scan.json

  jq -e '.scan.scanned_resources[] | select(.url | contains("bacon.html"))' 2>&1 >/dev/null <output/report_test_dynamic_urls_can_be_excluded_from_scan.json
  assert_not_equals "0" "$?" "expected dynamic url bacon.html to not be scanned, but it was"

  jq -e '.scan.scanned_resources[] | select(.url | contains("vegetables.html"))' 2>&1 >/dev/null <output/report_test_dynamic_urls_can_be_excluded_from_scan.json
  assert_not_equals "0" "$?" "expected dynamic url vegetables.html to not be scanned, but it was"

  grep '"uri": "http://nginx/bacon.html"' gl-dast-report.json >/dev/null
  assert_equals "1" "$?" "expected no vulnerabilities to be found for dynamic url bacon.html, but there were some"

  grep '"uri": "http://nginx/vegetables.html"' gl-dast-report.json >/dev/null
  assert_equals "1" "$?" "expected no vulnerabilities to be found for dynamic url vegetables.html, but there were some"

  ./verify-dast-schema.py output/report_test_dynamic_urls_can_be_excluded_from_scan.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"

  diff -u <(./normalize_dast_report.py expect/test_dynamic_urls_can_be_excluded_from_scan.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

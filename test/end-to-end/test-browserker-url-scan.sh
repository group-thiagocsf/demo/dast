#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_multi_page_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_url_baseline_scan() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_PATHS=/page1.html,/page2.html \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_browserker_url_baseline_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_browserker_url_baseline_scan.json

  diff -u <(./normalize_dast_report.py expect/test_browserker_url_baseline_scan.json) \
          <(./normalize_dast_report.py output/report_test_browserker_url_baseline_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_browserker_url_file_baseline_scan() {
  DAST_ROOT_DIR=${CI_PROJECT_DIR:-$PWD/../..}
  CI_PROJECT_DIR=${CI_PROJECT_DIR:-/builds}

  docker run --rm \
    -v "${DAST_ROOT_DIR}:${CI_PROJECT_DIR}" \
    -v "${PWD}":/output \
    --network test \
    --env CI_PROJECT_DIR="${CI_PROJECT_DIR}" \
    --env DAST_PATHS_FILE="test/end-to-end/fixtures/url-scan/paths_to_scan.txt" \
    --env DAST_BROWSER_SCAN=true \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_url_scan_using_builds_file_path.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_browserker_url_file_baseline_scan.json

  diff -u <(./normalize_dast_report.py expect/test_browserker_url_file_baseline_scan.json) \
          <(./normalize_dast_report.py output/report_test_browserker_url_file_baseline_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

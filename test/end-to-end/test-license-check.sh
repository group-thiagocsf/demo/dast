#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker rm --force opt-param-test >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_errors_when_dast_is_not_included_in_license() {
  docker run --rm \
      -v "${PWD}":/output \
      --env GITLAB_FEATURES="sast,dependency_scanning" \
      --network test \
      "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_errors_when_dast_is_not_included_in_license.log 2>&1
  assert_equals "1" "$?" "Expected to exit with errors"

  grep -q 'Error: Your GitLab project is not licensed for DAST.' output/test_errors_when_dast_is_not_included_in_license.log
  assert_equals "0" "$?" "Logged output differs from expectation"
}

test_dast_runs_as_normal_when_license_includes_dast() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env GITLAB_FEATURES="sast,dast,dependency_scanning" \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_dast_runs_as_normal_when_license_includes_dast.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q 'The following 6 URLs were scanned:' output/test_dast_runs_as_normal_when_license_includes_dast.log &&
  grep -q 'GET http://nginx/' output/test_dast_runs_as_normal_when_license_includes_dast.log &&
  grep -q 'POST http://nginx/myform' output/test_dast_runs_as_normal_when_license_includes_dast.log
  assert_equals "0" "$?" "Logged output differs from expectation"
}

test_dast_runs_as_normal_when_license_is_empty() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_dast_runs_as_normal_when_license_is_empty.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q 'The following 6 URLs were scanned:' output/test_dast_runs_as_normal_when_license_is_empty.log &&
  grep -q 'GET http://nginx/' output/test_dast_runs_as_normal_when_license_is_empty.log &&
  grep -q 'POST http://nginx/myform' output/test_dast_runs_as_normal_when_license_is_empty.log
  assert_equals "0" "$?" "Logged output differs from expectation"
}

#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null

  # start Rails Goat
  docker run --rm \
    --name railsgoat \
    --network test \
    -d \
    registry.gitlab.com/gitlab-org/security-products/dast/railsgoat-authentication-end-to-end-test >/dev/null

  true
}

teardown_suite() {
  docker rm -f railsgoat >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_railsgoat_authenticated_scan() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -j -d -t http://railsgoat:3001 \
    --auth-url http://railsgoat:3001/login? \
    --auth-verification-url http://railsgoat:3001/dashboard/home \
    --auth-username "ken@metacorp.com" \
    --auth-password "citrusblend" \
    --auth-username-field "email" \
    --auth-password-field "password" \
    --auth-submit-field "commit" \
    --auth-exclude-urls 'http://railsgoat:3001/logout' \
    >output/test_railsgoat_authenticated_scan.log 2>&1

  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q "citrusblend" output/test_railsgoat_authenticated_scan.log
  assert_not_equals "0" "$?" "Password present in logs"

  jq . < gl-dast-report.json > output/report_test_railsgoat_authenticated_scan.json

  jq '.scan.scanned_resources[].url' output/report_test_railsgoat_authenticated_scan.json | grep -q "http://railsgoat:3001/users/6"
  assert_equals "0" "$?" "Expected URL accessible after authentication to be included in scanned resources"
}

test_railsgoat_authentication_failure() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    "${BUILT_IMAGE}" /analyze -j -d -t http://railsgoat:3001 \
    --auth-url http://railsgoat:3001/login? \
    --auth-verification-url http://railsgoat:3001/dashboard/home \
    --auth-username "ken@metacorp.com" \
    --auth-password "wrongpassword" \
    --auth-username-field "email" \
    --auth-password-field "password" \
    --auth-submit-field "commit" \
    --auth-exclude-urls 'http://railsgoat:3001/logout' \
    >output/test_railsgoat_authentication_failure.log 2>&1

  assert_equals "1" "$?" "Expected to exit from failed authentication"

  grep -q "wrongpassword" output/test_railsgoat_authenticated_scan.log
  assert_not_equals "0" "$?" "Password present in logs"

  grep -q "DAST could not reach the auth verification URL" output/test_railsgoat_authentication_failure.log
  assert_equals "0" "$?" "Error message not printed as expected"
}
